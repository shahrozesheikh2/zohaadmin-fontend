import React, { Component } from 'react'
import { Link } from "react-router-dom";
export default class Sidebar extends Component {
    constructor(props) {
        super(props);
        this.state = { token:localStorage.getItem('zohausertoken'), 
            userrole:localStorage.getItem('zohauserrole'), 
            sidebarclass:'menu--link',
            sidebarclassactive:'menu--link active',
        };
    }
    render() {
        return (
          <nav className="vertical_nav">
          <div className="left_section menu_left" id="js-menu" >
            <div className="left_section">
              <ul>
                <li className="menu--item">
                <Link to='/dashboard' className={this.props.sidebarscreenname==='Dashboard'?this.state.sidebarclassactive:this.state.sidebarclass} title="Dashboard">
                    <i className="uil uil-apps menu--icon"></i>
                    <span className="menu--label">Dashboard</span>
                </Link>
                  
                </li>
                <li className="menu--item">
                  <Link  to='/buyer' className={this.props.sidebarscreenname==='Buyer'?this.state.sidebarclassactive:this.state.sidebarclass} title="Buyer">
                    <i className="uil uil-book-open menu--icon"></i>
                    <span className="menu--label">Buyer</span>
                  </Link>
                </li>
                <li className="menu--item">
                  <Link  to='/shop'  className={this.props.sidebarscreenname==='Shop'?this.state.sidebarclassactive:this.state.sidebarclass} title="Shop">
                    <i className='uil uil-play-circle menu--icon'></i>
                    <span className="menu--label">Shops</span>
                  </Link>
                </li>
                <li className="menu--item">
                  <Link  to='/wholesaler'  className={this.props.sidebarscreenname==='Wholesaler'?this.state.sidebarclassactive:this.state.sidebarclass} title="Wholesaler">
                    <i className='uil uil-align-right-justify menu--icon'></i>
                    <span className="menu--label">Wholesaler</span>
                  </Link>
                </li>
                <li className="menu--item">
                  <Link  to='/importer'  className={this.props.sidebarscreenname==='Importer'?this.state.sidebarclassactive:this.state.sidebarclass} title="Importer">
                    <i className='uil uil-clock-three  menu--icon'></i>
                    <span className="menu--label">Importer</span>
                  </Link>
                </li>
                <li className="menu--item">
                  <Link  to='/manufacturer'  className={this.props.sidebarscreenname==='Manufacturer'?this.state.sidebarclassactive:this.state.sidebarclass} title="Manufacturer">
                    <i className='uil uil-user-circle menu--icon'></i>
                    <span className="menu--label">Manufacturer</span>
                  </Link>
                </li>
                {(this.state.userrole==1) && (
                  <li className="menu--item">
                    <Link  to='/package'  className={this.props.sidebarscreenname==='Package'?this.state.sidebarclassactive:this.state.sidebarclass} title="Package">
                      <i className='uil uil-user-square menu--icon'></i>
                      <span className="menu--label">Package</span>
                    </Link>
                  </li>
                )}
                
                <li className="menu--item">
                  <Link  to='/country'  className={this.props.sidebarscreenname==='Country'?this.state.sidebarclassactive:this.state.sidebarclass} title="Country">
                    <i className='uil uil-user-square menu--icon'></i>
                    <span className="menu--label">Country</span>
                  </Link>
                </li>
                
                <li className="menu--item">
                  <Link  to='/city'  className={this.props.sidebarscreenname==='City'?this.state.sidebarclassactive:this.state.sidebarclass} title="City">
                    <i className='uil uil-user-square menu--icon'></i>
                    <span className="menu--label">City</span>
                  </Link>
                </li>
                
                <li className="menu--item">
                  <Link  to='/brand'  className={this.props.sidebarscreenname==='Brand'?this.state.sidebarclassactive:this.state.sidebarclass} title="Brands">
                    <i className='uil uil-user-square menu--icon'></i>
                    <span className="menu--label">Brands</span>
                  </Link>
                </li>
                <li className="menu--item">
                  <Link  to='/model'  className={this.props.sidebarscreenname==='Model'?this.state.sidebarclassactive:this.state.sidebarclass} title="Models">
                    <i className='uil uil-user-square menu--icon'></i>
                    <span className="menu--label">Models</span>
                  </Link>
                </li>
                {/* <li className="menu--item">
                  <Link  to='/year'  className={this.props.sidebarscreenname==='Year'?this.state.sidebarclassactive:this.state.sidebarclass} title="H.O.D/Workers">
                    <i className='uil uil-user-square menu--icon'></i>
                    <span className="menu--label">Year</span>
                  </Link>
                </li> */}
                <li className="menu--item">
                  <Link  to='/category'  className={this.props.sidebarscreenname==='Category'?this.state.sidebarclassactive:this.state.sidebarclass} title="Categories">
                    <i className='uil uil-user-square menu--icon'></i>
                    <span className="menu--label">Categories</span>
                  </Link>
                </li>
                <li className="menu--item">
                  <Link  to='/servicecategory'  className={this.props.sidebarscreenname==='ServiceCategory'?this.state.sidebarclassactive:this.state.sidebarclass} title="Service Category">
                    <i className='uil uil-user-square menu--icon'></i>
                    <span className="menu--label">Service Categories</span>
                  </Link>
                </li>
                {(this.state.userrole==1) && (
                  <li className="menu--item">
                    <Link  to='/adminUser'  className={this.props.sidebarscreenname==='AdminUser'?this.state.sidebarclassactive:this.state.sidebarclass} title="Admin Users">
                      <i className='uil uil-user menu--icon'></i>
                      <span className="menu--label">Admin Users</span>
                    </Link>
                  </li>
                )}
                <li className="menu--item">
                  <Link  to='/otp'  className={this.props.sidebarscreenname==='Otp'?this.state.sidebarclassactive:this.state.sidebarclass} title="Otp">
                    <i className='uil uil-users-alt menu--icon'></i>
                    <span className="menu--label">OTP</span>
                  </Link>
                </li>
                <li className="menu--item">
                  <Link  to='/editprofile'  className={this.props.sidebarscreenname==='EditProfile'?this.state.sidebarclassactive:this.state.sidebarclass} title="EditProfile">
                    <i className='uil uil-users-alt menu--icon'></i>
                    <span className="menu--label">Edit Profile</span>
                  </Link>
                </li>
                <li className="menu--item" >
                  <Link to='/signin' onClick={()=>{ localStorage.clear(); }} className={this.props.sidebarscreenname==='Logout'?this.state.sidebarclassactive:this.state.sidebarclass} title="Logout" title="Logout">
                    <i className='uil uil-user-square menu--icon'></i>
                    <span className="menu--label">Logout</span>
                  </Link>
                </li>
              </ul>
            </div>
          </div>
        </nav>
      )
    }
}
