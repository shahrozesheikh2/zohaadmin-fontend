import React, { Component } from 'react'

export default class Footer extends Component {
    render() {
        return (
          <footer className="footer mt-40">
          <div className="container-fluid">
            <div className="row">			
              <div className="col-lg-12">
                <div className="footer_bottm mt-0">
                  <div className="row">
                    <div className="col-md-12">
                    <p style={{color:'white'}}>© 2021 <strong>ZOHA</strong> All Rights Reserved.</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </footer>    
        )
    }
}
