import React, { Component } from 'react';
import { Switch } from "react-router-dom";
import { Route } from "react-router-dom";
import './App.css';
import Dashboard from './Screens/Dashboard';

import AdminUser from './Screens/AdminUsers';
import Buyer from './Screens/Buyer';
import Shop from './Screens/Shop';
import Wholesaler from './Screens/Wholesaler';
import Importer from './Screens/Importer';
import Manufacturer from './Screens/Manufacturer';
import EditProfile from './Screens/EditProfile';
import Signin from './Screens/Signin';
import Country from './Screens/Country';
import City from './Screens/City';
import Brand from './Screens/Brand';
import Category from './Screens/Category';
import Model from './Screens/Model';
import Year from './Screens/Year';
import Package from './Screens/Package';
import Otp from './Screens/Otp';
import ServiceCategory from './Screens/ServiceCategory';
export default class App extends Component {
  constructor(props){
    super(props);
    global.url='http://zohaqaphase2-env.eba-3kera2wc.ap-southeast-1.elasticbeanstalk.com';
  }
  render() {
    return (
      <Switch>
        <Route path="/" exact component={Signin} />
        <Route path="/dashboard" component={Dashboard} />
        <Route path="/buyer" component={Buyer} />
        <Route path="/shop" component={Shop} />
        <Route path="/wholesaler" component={Wholesaler} />
        <Route path="/importer" component={Importer} />
        <Route path="/manufacturer" component={Manufacturer} />
        <Route path="/editprofile" component={EditProfile} />
        <Route path="/adminUser" component={AdminUser} />
        <Route path="/signin" component={Signin} />
        <Route path="/country" component={Country} />
        <Route path="/city" component={City} />
        <Route path="/brand" component={Brand} />
        <Route path="/category" component={Category} />
        <Route path="/servicecategory" component={ServiceCategory} />
        <Route path="/model" component={Model} />
        <Route path="/year" component={Year} />
        <Route path="/package" component={Package} />
        <Route path="/otp" component={Otp} />
      </Switch>
    )
  }
}
