import React, { Component } from 'react';
import Footer from '../components/Footer';
import Header from '../components/Header';
import Sidebar from '../components/Sidebar';
import axios from "axios";
export default class EditProfile extends Component {
    constructor(props) {
        super(props);
        this.state = { token:localStorage.getItem('zohausertoken'),
            countrydata:[],
            oldpswd:'',
            newpswd:''
        };
    }
    changePassword=()=>{
        var header={
            "Authorization":this.state.token
        }
        var body={
            "oldPassword": this.state.oldpswd,
            "newPassword": this.state.newpswd
        }
        const url = global.url+'/api/admin/change-password';
        axios.post(url,body,{headers:header}).then(response => {
            this.setState({oldpswd:'',newpswd:''})
            alert(response.data.message)
        }).catch(err=>{
            alert(err.response.data.message)
        });
    }
  render() {
    return (
        <>
        <Header/>
        <Sidebar sidebarscreenname={'EditProfile'}/>
        <div className="wrapper">
        <div className="sa4d25">
            <div className="container-fluid">
                <div class="sign_in_up_bg">
                    <div class="container">
                        <div class="row justify-content-lg-center justify-content-md-center">
                        
                            <div class="col-lg-6 col-md-8">
                                <div class="sign_form">
                                        <div class="ui search focus mt-15">
                                            <div class="ui left icon input swdh95">
                                                <input class="prompt srch_explore" type="password" name="oldpswd" placeholder="Old Password" 
                                                value={this.state.oldpswd} onChange={(e) => {this.setState({oldpswd:e.target.value})}} />															
                                                <i class="uil uil-envelope icon icon2"></i>
                                            </div>
                                        </div>
                                        <div class="ui search focus mt-15">
                                            <div class="ui left icon input swdh95">
                                                <input class="prompt srch_explore" type="password" name="newpswd" placeholder="New Password" 
                                                value={this.state.newpswd} onChange={(e) => {this.setState({newpswd:e.target.value})}} />
                                                <i class="uil uil-key-skeleton-alt icon icon2"></i>
                                            </div>
                                        </div>
                                        <button class="login-btn"  onClick={() => this.changePassword()}>Change Password</button>
                                </div>
                            </div>				
                        </div>				
                    </div>				
                </div>
            </div>
		</div>
	</div>
        <Footer/>
        </>
    );
  }
}
