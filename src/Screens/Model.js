import React, { Component } from 'react'
import Footer from '../components/Footer';
import Header from '../components/Header';
import Sidebar from '../components/Sidebar';
import axios from "axios";
import Select from 'react-select';
export default class Model extends Component {
    constructor(props) {
        super(props);
        this.state = { token:localStorage.getItem('zohausertoken'),
            modeldata:[],
            branddata:[],
            viewmodel:'',
            editcode:'',
            editname:'',
            editphonecode:'',
            name:'',
            code:'',
            phonecode:'',
            brandid:'',
            defaultbrandid:1,
            editid:''
        };
        this.getmodel();
        this.getbrand();
    }
    componentDidMount() {
    }
    getbrand=()=>{
        
        const url = global.url+'/api/brand?offset=0&limit=100';
        axios.get(url).then(response => {
            console.log('====================================');
            console.log(response.data.result.data);
            console.log('====================================');
            this.setState({branddata:response.data.result.data})
        }).catch(err=>{
        console.log(err)
        });
    }
    getmodel=()=>{
        this.setState({ editname:'',name:''})
        const url = global.url+'/api/model?offset=0&limit=500&id='+this.state.defaultbrandid;
        axios.get(url).then(response => {
            console.log('====================================');
            console.log(response.data.result.data);
            console.log('====================================');
            this.setState({modeldata:response.data.result.data})
        }).catch(err=>{
        console.log(err)
        });
    }
    viewmodel=(id)=>{
        var header={
            "Authorization":this.state.token
        }
        const url = global.url+'/api/model/'+id;
        axios.get(url,{headers:header}).then(response => {
            console.log('====================================');
            console.log(response.data.result.data);
            console.log('====================================');
            this.setState({viewmodel:response.data.result.data})
        }).catch(err=>{
        console.log(err)
        });
    }
    deletemodel=(id)=>{
        var header={
            "Authorization":this.state.token
        }
        const url = global.url+'/api/model/'+id;
        axios.delete(url,{headers:header}).then(response => {
            console.log('====================================');
            console.log(response.data.result.data);
            console.log('====================================');
            this.getmodel();
        }).catch(err=>{
        console.log(err);
        alert(err.response.data.message)
        });
    }
    updatemodel=()=>{
        var header={
            "Authorization":this.state.token
        }
        var body={
            "name": this.state.editname,
            "brandId": this.state.brandid
        }
        const url = global.url+'/api/model/'+this.state.editid;
        axios.put(url,body,{headers:header}).then(response => {
            console.log('====================================');
            console.log(response.data);
            console.log('====================================');
            this.getmodel();
        }).catch(err=>{
        console.log(err);
        alert(err.response.data.message)
        });
    }
    addmodel=()=>{
        var header={
            "Authorization":this.state.token
        }
        var body={
            "name": this.state.name,
            "brandId": this.state.brandid
        }
        const url = global.url+'/api/model/';
        axios.post(url,body,{headers:header}).then(response => {
            console.log('====================================');
            console.log(response.data);
            console.log('====================================');
            this.getmodel();
        }).catch(err=>{
        console.log(err);
        alert(err.response.data.message)
        });
    }
    render() {
        return (
            <>
            <Header/>
            <Sidebar sidebarscreenname={'Model'}/>
            <div className="wrapper">
                <div className="sa4d25">
                    <div className="container-fluid">			
                        <div className="row">
                            <div className="col-lg-12">	
                                <h2 className="st_title"><i className="uil uil-play-circle"></i>Model</h2>
                            </div>	
                            <div className="col-lg-4">	
                            </div>
                            <div className="col-lg-4">	
                            </div>
                        </div>

                        <div className="col-lg-12 col-md-12">	
                            <div className="ui search focus mt-20 lbel25">
                                <h2 className="st_title">Select Brand</h2>
                                <Select options={this.state.branddata.map((item) => {
                                    return { value: item.id, label: item.name };
                                })}
                                onChange={entry => {
                                    console.log('====================================');
                                    console.log(entry);
                                    console.log('====================================');
                                    this.setState({ defaultbrandid: entry.value });
                                    setTimeout(() => {
                                        this.getmodel();
                                    }, 100);
                                }}
                                defaultValue={{ label: "Toyota", value: 1 }}
                                className="basic-multi-select"
                                classNamePrefix="select"
                                />
                            </div>										
                        </div>
                        <div className="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                            <div className="panel panel-default">
                                <div className="panel-heading" role="tab" id="headingOne">
                                    <div className="panel-title adcrse1250">
                                        <a className="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                            Add Model
                                        </a>
                                    </div>
                                </div>
                                <div id="collapseOne" className="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                    <div className="panel-body adcrse_body">
                                        <div className="row">
                                            <div className="col-lg-12">
                                                <div className="discount_form">
                                                    <div className="row">
                                                        <div className="col-lg-12 col-md-12">	
                                                            <div className="ui search focus mt-20 lbel25">
                                                                <label>Model Name*</label>
                                                                <div className="ui left icon input swdh19">
                                                                    <input className="prompt srch_explore" placeholder="Model Name" type="text" 
                                                                    value={this.state.name} onChange={(e) => {this.setState({name:e.target.value})}} required/>															
                                                                </div>
                                                            </div>										
                                                        </div>
                                                        <div className="col-lg-12 col-md-12">	
                                                            <div className="ui search focus mt-20 lbel25">
                                                                <label>Brand*</label>
                                                                <Select options={this.state.branddata.map((item) => {
                                                                    return { value: item.id, label: item.name };
                                                                })}
                                                                onChange={entry => {
                                                                    console.log('====================================');
                                                                    console.log(entry);
                                                                    console.log('====================================');
                                                                    this.setState({ brandid: entry.value });
                                                                }}
                                                                className="basic-multi-select"
                                                                classNamePrefix="select"
                                                                />
                                                            </div>										
                                                        </div>
                                                        <div className="col-lg-12 col-md-12" onClick={()=>{this.addmodel()}}>	
                                                            <button className="discount_btn" type="submit">Save</button>										
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div className="table-responsive mt-30">
                            <table className="table ucp-table">
                                <thead className="thead-s">
                                    <tr>
                                        <th className="text-center" scope="col">#</th>
                                        <th className="text-center" scope="col">Model Name</th>
                                        <th className="text-center" scope="col">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.state.modeldata.map((item,index)=>{
                                        return(
                                        <tr key={index}>    
                                            <td className="text-center">{index+1}</td>
                                            <td className="text-center">{item.name}</td>
                                            <td className="text-center">
                                                <a href="javascript:void(0)" title="Edit" className="gray-s"  data-toggle="modal" data-target="#myModal" onClick={()=>{this.setState({
                                                    editid:item.id,
                                                    editname:item.name
                                                })}}><i className="uil uil-edit-alt"></i></a>
                                                {/* <a href="javascript:void(0)" title="Delete" className="gray-s" onClick={()=>{this.deletemodel(item.id)}}><i className="uil uil-trash-alt"></i></a> */}
                                            </td>
                                        </tr>
                                        )
                                    })}
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>


            <div className="modal fade" id="myModal" role="dialog" >
                <div className="modal-dialog modal-lg" style={{maxWidth:1100}}>
                    <div className="modal-content">
                        <div className="modal-header">
                        <button type="button" className="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div className="modal-body" style={{paddingBottom:30}}>
                            <div className="col-md-12">
                                <h3 className="text-center">Edit Model</h3>
                            </div>
                            <div className="col-lg-12 col-md-12">	
                                <div className="ui search focus mt-20 lbel25">
                                    <label>Model Name*</label>
                                    <div className="ui left icon input swdh19">
                                        <input className="prompt srch_explore" placeholder="Model Name" type="text" 
                                        value={this.state.editname} onChange={(e) => {this.setState({editname:e.target.value})}} required/>															
                                    </div>
                                </div>										
                            </div>
                            <div className="col-lg-12 col-md-12">	
                                <div className="ui search focus mt-20 lbel25">
                                    <label>Brand*</label>
                                    <Select options={this.state.branddata.map((item) => {
                                        return { value: item.id, label: item.name };
                                    })}
                                    onChange={entry => {
                                        console.log('====================================');
                                        console.log(entry);
                                        console.log('====================================');
                                        this.setState({ brandid: entry.value });
                                    }}
                                    className="basic-multi-select"
                                    classNamePrefix="select"
                                    />
                                </div>										
                            </div>
                            <div className="col-lg-12 col-md-12" onClick={()=>{this.updatemodel()}}>	
                                <button className="discount_btn"  data-dismiss="modal">Save</button>										
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <Footer/>
            </>
        )
    }
}
