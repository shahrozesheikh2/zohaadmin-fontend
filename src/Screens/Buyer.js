import React, { Component } from 'react'
import Footer from '../components/Footer';
import Header from '../components/Header';
import Sidebar from '../components/Sidebar'; 
import axios from "axios";
import ReactPaginate from 'react-paginate';
export default class Buyer extends Component {
    constructor(props) {
        super(props);
        this.state = { token:localStorage.getItem('zohausertoken'),
            buyerdata:[],
            viewbuyer:'',
            editcode:'',
            editname:'',
            editphonecode:'',
            buyershowdata:'',
            name:'',
            code:'',
            phonecode:'',
            totalpages:0,
            start:0,
            searchname:'',
            searchphone:'',
            countrydata:[],
            citydata:[]
            // searchcity:0
        };
        this.getbuyer();
        this.getCountry();
        this.getcity();
    }
    getCountry=()=>{
        const url = global.url+'/api/country?offset=0&limit=100';
        axios.get(url).then(response => {
            console.log('====================================');
            console.log(response.data.result.data);
            console.log('====================================');
            this.setState({countrydata:response.data.result.data})
        }).catch(err=>{
        console.log(err)
        });
    }
    getcity=()=>{
        const url = global.url+'/api/city?offset='+this.state.start+'&limit=500&id=1&cityName='+this.state.searchcity;
        axios.get(url).then(response => {
            console.log('====================================');
            console.log(response.data);
            console.log('====================================');
            this.setState({citydata:response.data.result.data});
        }).catch(err=>{
        console.log(err)
        });
    }
    getbuyer=()=>{
        var header={
            "Authorization":this.state.token
        }
        // &cityId='+this.state.searchcity+'
        const url = global.url+'/api/user/?fullName='+this.state.searchname+'&phoneNumber='+this.state.searchphone+'&roleId=2&statusIds[]=2&statusIds[]=3&statusIds[]=4&offset='+this.state.start+'&limit=10';
        axios.get(url,{headers:header}).then(response => {
            console.log('====================================');
            console.log(response.data.result.data);
            console.log('====================================');
            this.setState({buyerdata:response.data.result.data})
            this.setState({totalpages:Math.ceil(response.data.result.totalCount/10)})
        }).catch(err=>{
        console.log(err)
        });
    }
    viewbuyer=(id)=>{
        const url = global.url+'/api/buyer/'+id;
        axios.get(url).then(response => {
            console.log('====================================');
            console.log(response.data.result.data);
            console.log('====================================');
            this.setState({viewbuyer:response.data.result.data})
        }).catch(err=>{
        console.log(err)
        });
    }
    deletebuyer=(id)=>{
        const url = global.url+'/api/buyer/'+id;
        axios.get(url).then(response => {
            console.log('====================================');
            console.log(response.data.result.data);
            console.log('====================================');
            this.getbuyer();
        }).catch(err=>{
        console.log(err);
        alert(err.response.data.message)
        });
    }
    updatebuyer=(id)=>{
        var body={
            "buyerName": this.state.editname,
            "buyerCode": this.state.editcode,
            "phoneCode": this.state.editphonecode
        }
        const url = global.url+'/api/buyer/'+id;
        axios.get(url,body).then(response => {
            console.log('====================================');
            console.log(response.data);
            console.log('====================================');
            this.getbuyer();
        }).catch(err=>{
        console.log(err);
        alert(err.response.data.message)
        });
    }
    addbuyer=()=>{
        var body={
            "buyerName": this.state.name,
            "buyerCode": this.state.code,
            "phoneCode": this.state.phonecode
        }
        const url = global.url+'/api/buyer/';
        axios.get(url,body).then(response => {
            console.log('====================================');
            console.log(response.data);
            console.log('====================================');
            this.getbuyer();
        }).catch(err=>{
        console.log(err);
        alert(err.response.data.message)
        });
    }
    activate=(id,status)=>{
        var header={
            "Authorization":this.state.token
        }
        const url = global.url+'/api/user/activate/'+id;
        var body={}
        if(status=="Verified"){
            axios.delete(url,{headers:header}).then(response => {
                console.log('====================================');
                console.log(response.data);
                console.log('====================================');
                this.getbuyer();
            }).catch(err=>{
                console.log(err);
                alert(err.response.data.message)
            });
        }
        else{
            axios.patch(url,body,{headers:header}).then(response => {
                console.log('====================================');
                console.log(response.data);
                console.log('====================================');
                this.getbuyer();
            }).catch(err=>{
                console.log(err);
                alert(err.response.data.message)
            });
        }
        
    }
    render() {
        return (
            <>
            <Header/>
            <Sidebar sidebarscreenname={'Buyer'}/>
            <div className="wrapper">
                <div className="sa4d25">
                    <div className="container-fluid">			
                        <div className="row">
                            <div className="col-lg-3">	
                                <h2 className="st_title"><i className="uil uil-play-circle"></i>Buyer</h2>
                            </div>
                            <div className="col-lg-3">	</div>	
                            <div className="col-lg-3">	
                                <div className="ui search focus">
                                    <div className="ui left icon input swdh11 swdh15">
                                        <input className="prompt srch_explore" type="text" placeholder="Name" value={this.state.searchname} onChange={(e) => {this.setState({searchname:e.target.value});
                                    setTimeout(() => {
                                        this.getbuyer();
                                    }, 100);}}/>
                                        
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-3">	
                                <div className="ui search focus">
                                    <div className="ui left icon input swdh11 swdh15">
                                        <input className="prompt srch_explore" type="text" placeholder="Phone" value={this.state.searchphone} onChange={(e) => {this.setState({searchphone:e.target.value});
                                    setTimeout(() => {
                                        this.getbuyer();
                                    }, 100);}}/>
                                        
                                    </div>
                                </div>
                            </div>	
                            {/* <div className="col-lg-3">	
                                <div className="ui search focus">
                                    <div className="ui left icon input swdh11 swdh15">
                                        <input className="prompt srch_explore" type="text" placeholder="City Name" value={this.state.searchcity} onChange={(e) => {this.setState({searchcity:e.target.value});
                                    setTimeout(() => {
                                        this.getbuyer();
                                    }, 100);}}/>
                                        
                                    </div>
                                </div>
                            </div> */}
                        </div>
                        <div className="table-responsive mt-30">
                            <table className="table ucp-table">
                                <thead className="thead-s">
                                    <tr>
                                        {/* <th className="text-center" scope="col">Created Date</th> */}
                                        <th className="text-center" scope="col">Name</th>
                                        <th className="text-center" scope="col">Phone</th>
                                        <th className="text-center" scope="col">Status</th>
                                        <th className="text-center" scope="col">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                {this.state.buyerdata.map((item,index)=>{
                                        return(
                                    <tr key={index}>
                                        {/* <td className="text-center">{item.createdAt}</td> */}
                                        <td className="text-center">{item.fullName}</td>
                                        <td className="text-center">{item.phoneNumber}</td>
                                        <td className="text-center">
                                        <a href="javascript:void(0)" title="Delete" style={{color:item.statusChar!="Verified"?'red':'green'}} onClick={()=>{this.activate(item.id,item.statusChar)}}>
                                            {item.statusChar}
                                        </a>
                                        </td>
                                        <td className="text-center">
                                            <a href="javascript:void(0)" title="View" data-toggle="modal" data-target="#myModal" className="gray-s" 
                                            onClick={()=>{this.setState({
                                                buyershowdata:item
                                            })
                                            console.log('====================================');
                                            console.log(item);
                                            console.log('====================================');
                                            }}><i className="uil uil-eye"></i></a>
                                        </td>
                                    </tr>
                                    )
                                })}
                                </tbody>
                            </table>
                        </div>
                        <ReactPaginate
                                previousLabel={'PRE'}
                                nextLabel={'NEXT'}
                                breakLabel={'...'}
                                breakClassName={'break-me'}
                                pageCount={this.state.totalpages}
                                marginPagesDisplayed={2}
                                pageRangeDisplayed={5}
                                onPageChange={(data)=>{
                                console.log(data);
                                this.setState({start:data.selected});
                                setTimeout(() => {
                                    this.getbuyer();
                                }, 200);
                                }}
                                containerClassName={'pagination'}
                                subContainerClassName={'pages pagination'}
                                activeClassName={'active'}
                            />
                        
                    </div>
                </div>
            </div>

            <div className="modal fade" id="myModal" role="dialog" >
                <div className="modal-dialog modal-lg" style={{maxWidth:1100}}>
                    <div className="modal-content">
                        <div className="modal-header">
                        <h3 className="text-center">Buyer</h3>
                        <button type="button" className="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div className="modal-body" style={{paddingBottom:30}}>
                            {/* <div className="col-md-12">
                                <div className="col-md-3" style={{float:'right'}}>
                                    <button className="discount_btn" style={{marginTop:0}} data-dismiss="modal">View Chat</button>		
                                </div>		
                            </div> */}
                            <div className="col-lg-12 col-md-12" style={{float:'left'}}>	
                                <div className="ui search focus mt-20 lbel25">
                                    <label>Buyer Name*</label>
                                    <div className="ui left icon input swdh19">
                                        <input className="prompt srch_explore" placeholder="Buyer Name" type="text" disabled
                                        value={this.state.buyershowdata.fullName}/>															
                                    </div>
                                </div>	
                                <div className="ui search focus mt-20 lbel25">
                                    <label>Buyer Phone Number*</label>
                                    <div className="ui left icon input swdh19">
                                        <input className="prompt srch_explore" placeholder="Buyer Name" type="text" disabled
                                        value={this.state.buyershowdata.phoneNumber}/>															
                                    </div>
                                </div>	
                                <div className="ui search focus mt-20 lbel25">
                                    <label>Image*</label>
                                    <div className="ui left icon input swdh19">
                                    <img src={this.state.buyershowdata.awsUrl} alt="" style={{width:100}}/>													
                                    </div>
                                </div>									
                            </div>
                            {/* <div className="col-lg-12 col-md-12" onClick={()=>{this.updatecity()}}>	
                                <button className="discount_btn"  data-dismiss="modal">Save</button>										
                            </div> */}
                        </div>
                    </div>
                </div>
            </div>
            
            <Footer/>
            </>
        )
    }
}
