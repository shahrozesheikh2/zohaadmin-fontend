import React, { Component } from 'react'
import Footer from '../components/Footer';
import Header from '../components/Header';
import Sidebar from '../components/Sidebar';
import axios from "axios";
import ReactPaginate from 'react-paginate'; 
import Select from 'react-select';
export default class Shop extends Component {
    constructor(props) {
        super(props);
        this.state = { token:localStorage.getItem('zohausertoken'),
            shopdata:[],
            viewshop:'',
            editcode:'',
            editname:'',
            editphonecode:'',
            name:'',
            code:'',
            phonecode:'',
            totalpages:0,
            start:0,
            searchshopname:'',
            searchusername:'',
            searchphone:'',
            searchstatus:'',
            sellershowdata:'',
            products:[],
            packagedata:[],
            packageid:'',
            chatdata:[],
            chatviewdata:[],
            services:[]
        };
        this.getshop();
        this.getpackage()
        
    }
    componentDidMount() {
    }
    getpackage=()=>{
        const url = global.url+'/api/package?offset=0&limit=100';
        axios.get(url).then(response => {
            console.log('====================================');
            console.log(response.data.result.data);
            console.log('====================================');
            this.setState({packagedata:response.data.result.data})
        }).catch(err=>{
            console.log(err)
        });
    }
    getshop=()=>{
        var header={
            "Authorization":this.state.token
        }
        const url = global.url+'/api/user/?roleId=1&statusIds[]=1&statusIds[]=2&statusIds[]=3&statusIds[]=4&shopName='+this.state.searchshopname+'&phoneNumber='+this.state.searchphone+'&fullName='+this.state.searchusername+'&offset='+this.state.start+'&limit=10';
        axios.get(url,{headers:header}).then(response => {
            console.log('====================================');
            console.log(response.data.result.data);
            console.log('====================================');
            this.setState({shopdata:response.data.result.data});
            this.setState({totalpages:Math.ceil(response.data.result.totalCount/10)})
        }).catch(err=>{
        console.log(err)
        });
    }
    viewshop=(id)=>{
        const url = global.url+'/api/shop/'+id;
        axios.get(url).then(response => {
            console.log('====================================');
            console.log(response.data.result.data);
            console.log('====================================');
            this.setState({viewshop:response.data.result.data})
        }).catch(err=>{
        console.log(err)
        });
    }
    activate=(id,status)=>{
        var header={
            "Authorization":this.state.token
        }
        const url = global.url+'/api/user/activate/'+id;
        var body={}
        if(status=="Verified"){
            axios.delete(url,{headers:header}).then(response => {
                console.log('====================================');
                console.log(response.data);
                console.log('====================================');
                this.getshop();
            }).catch(err=>{
                console.log(err);
                alert(err.response.data.message)
            });
        }
        else{
            axios.patch(url,body,{headers:header}).then(response => {
                console.log('====================================');
                console.log(response.data);
                console.log('====================================');
                this.getshop();
            }).catch(err=>{
                console.log(err);
                alert(err.response.data.message)
            });
        }
        
    }
    verifydoc=(id,shopid,packageid)=>{
        var header={
            "Authorization":this.state.token
        }
        var body={
            shopId:shopid,
            packageId:packageid
        }
        const url = global.url+'/api/shop/toggle-document-status/'+shopid;
        // if(status=="Verified"){
        //     axios.delete(url,body,{headers:header}).then(response => {
        //         console.log('====================================');
        //         console.log(response.data);
        //         console.log('====================================');
        //         this.getshop();
        //     }).catch(err=>{
        //         console.log(err);
        //         alert(err.response.data.message)
        //     });
        // }
        // else{
            axios.patch(url,body,{headers:header}).then(response => {
                console.log('====================================');
                console.log(response.data);
                console.log('====================================');
                this.getshop();
            }).catch(err=>{
                console.log(err);
                alert(err.response.data.message)
            });
        // }
        
    }
    getproducts=(id)=>{
        var header={
            "Authorization":this.state.token
        }
        const url = global.url+'/api/product/'+id+'?offset=0&limit=900';
        axios.get(url,{headers:header}).then(response => {
            console.log('====================================');
            console.log(response.data.result.data);
            console.log('====================================');
            this.setState({products:response.data.result.data});
        }).catch(err=>{
        console.log(err)
        });
    }
    getservices=(id)=>{
        var header={
            "Authorization":this.state.token
        }
        const url = global.url+'/api/service?offset=0&limit=900&shopId='+id;
        axios.get(url,{headers:header}).then(response => {
            console.log('====================================');
            console.log(response.data.result.data);
            console.log('====================================');
            this.setState({services:response.data.result.data});
        }).catch(err=>{
        console.log(err)
        });
    }
    changepackage=()=>{
        var header={
            "Authorization":this.state.token
        }
        var body={
            "shopId" : this.state.sellershowdata.Shop.id,
            "packageId": this.state.packageid
        }
        const url = global.url+'/api/shop/change-package';
        console.log('====================================');
        console.log(body);
        console.log('====================================');
        axios.post(url,body,{headers:header}).then(response => {
            console.log('====================================');
            console.log(response);
            console.log('====================================');
            this.getshop();
        }).catch(err=>{
        console.log(err)
        });
    }
    getchat=(id)=>{
        var header={
            "Authorization":this.state.token
        }
        const url = global.url+'/api/chat?user1Id='+id;
        axios.get(url,{headers:header}).then(response => {
            console.log('====================================');
            console.log(response.data.result.data);
            console.log('====================================');
            this.setState({chatdata:response.data.result.data})
        }).catch(err=>{
        console.log(err)
        });
    }
    render() {
        return (
            <>
            <Header/>
            <Sidebar sidebarscreenname={'Shop'}/>
            <div className="wrapper">
                <div className="sa4d25">
                    <div className="container-fluid">			
                        <div className="row">
                            <div className="col-lg-12">	
                                <h2 className="st_title"><i className="uil uil-play-circle"></i>Shop</h2>
                            </div>	
                            <div className="col-lg-3">	
                                <div className="ui search focus">
                                    <div className="ui left icon input swdh11 swdh15">
                                        <input className="prompt srch_explore" type="text" placeholder="Shop Name" value={this.state.searchshopname} onChange={(e) => {this.setState({searchshopname:e.target.value});
                                    setTimeout(() => {
                                        this.getshop();
                                    }, 100);}}/>
                                        
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-3">	
                                <div className="ui search focus">
                                    <div className="ui left icon input swdh11 swdh15">
                                        <input className="prompt srch_explore" type="text" placeholder="Username" value={this.state.searchusername} onChange={(e) => {this.setState({searchusername:e.target.value});
                                    setTimeout(() => {
                                        this.getshop();
                                    }, 100);}}/>
                                        
                                    </div>
                                </div>
                            </div>	
                            <div className="col-lg-3">	
                                <div className="ui search focus">
                                    <div className="ui left icon input swdh11 swdh15">
                                        <input className="prompt srch_explore" type="text" placeholder="Phone" value={this.state.searchphone} onChange={(e) => {this.setState({searchphone:e.target.value});
                                    setTimeout(() => {
                                        this.getshop();
                                    }, 100);}}/>
                                        
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-3">	
                                <div className="ui search focus">
                                    <div className="ui left icon input swdh11 swdh15">
                                        <input className="prompt srch_explore" type="text" placeholder="Status" value={this.state.searchstatus} onChange={(e) => {this.setState({searchstatus:e.target.value});
                                    setTimeout(() => {
                                        this.getshop();
                                    }, 100);}}/>
                                        
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div className="table-responsive mt-30">
                            <table className="table ucp-table">
                                <thead className="thead-s">
                                    <tr>
                                        {/* <th className="text-center" scope="col">Created Date</th> */}
                                        <th className="text-center" scope="col">Shop Name</th>
                                        <th className="text-center" scope="col">Username</th>
                                        <th className="text-center" scope="col">Phone</th>
                                        <th className="text-center" scope="col">Status</th>
                                        <th className="text-center" scope="col">Document Status</th>
                                        <th className="text-center" scope="col">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                {this.state.shopdata.map((item,index)=>{
                                        return(
                                    <tr key={index}>
                                        {/* <td className="text-center">{item.createdAt}</td> */}
                                        <td className="text-center">{item.Shop.name}</td>
                                        <td className="text-center">{item.fullName}</td>
                                        <td className="text-center">{item.phoneNumber}</td>
                                        <td className="text-center">
                                        <a href="javascript:void(0)" title="Verify" style={{color:item.statusChar!='Verified'?'red':'green'}} onClick={()=>{
                                            if(item.statusChar!='Pending'){
                                                this.activate(item.id,item.statusChar)
                                            }
                                            else{
                                                alert("You Can't Edit Pending Status" )
                                            }
                                            }}>
                                            {item.statusChar}
                                        </a>
                                        </td>
                                        <td className="text-center">
                                        <a href="javascript:void(0)" title="Verify Documents" style={{color:item.Shop.documentStatus!='Verified'?'red':'green'}} onClick={()=>{
                                            if(item.Shop.documentStatus!='Pending'){
                                                this.verifydoc(item.id,item.Shop.id,item.Shop.packageId)
                                            }
                                            else{
                                                alert("You Can't Edit Pending Status" )
                                            }
                                            }}>
                                            {item.Shop.documentStatus}
                                        </a>
                                        </td>
                                        <td className="text-center">
                                        <a href="javascript:void(0)" title="View" data-toggle="modal" data-target="#myModal" className="gray-s" 
                                            onClick={()=>{this.setState({
                                                sellershowdata:item,
                                                products:[]
                                            })
                                            this.getproducts(item.Shop.id);
                                            this.getservices(item.Shop.id);
                                            
                                        }}>
                                            <i className="uil uil-eye"></i>
                                        </a>   

                                        <a href="javascript:void(0)" title="Package Update" data-toggle="modal" data-target="#myPackageModal" className="gray-s" 
                                            onClick={()=>{
                                                this.setState({ sellershowdata:item })
                                            }}>
                                            <i className="uil uil-package"></i>
                                        </a>   

                                        <a href="javascript:void(0)" title="Chat" data-toggle="modal" data-target="#myChatModal" className="gray-s" 
                                            onClick={()=>{
                                                this.setState({ sellershowdata:item,chatdata:[],chatviewdata:[] });
                                                this.getchat(item.id);
                                            }}>
                                            <i className="uil uil-chat"></i>
                                        </a>  
                                        
                                        </td>
                                    </tr>
                                    )
                                })}
                                </tbody>
                            </table>
                        </div>
                        <ReactPaginate
                            previousLabel={'PRE'}
                            nextLabel={'NEXT'}
                            breakLabel={'...'}
                            breakClassName={'break-me'}
                            pageCount={this.state.totalpages}
                            marginPagesDisplayed={2}
                            pageRangeDisplayed={5}
                            onPageChange={(data)=>{
                            console.log(data);
                            this.setState({start:data.selected});
                            setTimeout(() => {
                                this.getshop();
                            }, 200);
                            }}
                            containerClassName={'pagination'}
                            subContainerClassName={'pages pagination'}
                            activeClassName={'active'}
                        />
                    </div>
                </div>
            </div>
            <div className="modal fade" id="myModal" role="dialog" >
                <div className="modal-dialog modal-lg" style={{maxWidth:1100}}>
                    <div className="modal-content">
                        <div className="modal-header">
                        <h3 className="text-center">Shop</h3>
                        <button type="button" className="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div className="modal-body" style={{paddingBottom:30}}>
                            {/* <div className="col-md-12">
                                <div className="col-md-3" style={{float:'right'}}>
                                    <button className="discount_btn" style={{marginTop:0}} data-dismiss="modal">View Chat</button>		
                                </div>		
                            </div> */}
                            <div className="col-lg-12 col-md-12" style={{float:'left'}}>	
                                <div className="ui search focus mt-20 lbel25">
                                    <label>Owner Name*</label>
                                    <div className="ui left icon input swdh19">
                                        <input className="prompt srch_explore" placeholder="Buyer Name" type="text" disabled
                                        value={this.state.sellershowdata.fullName}/>															
                                    </div>
                                </div>	
                                <div className="ui search focus mt-20 lbel25">
                                    <label>Onwer Phone Number*</label>
                                    <div className="ui left icon input swdh19">
                                        <input className="prompt srch_explore" placeholder="Buyer Name" type="text" disabled
                                        value={this.state.sellershowdata.phoneNumber}/>															
                                    </div>
                                </div>
                                
                                <div className="ui search focus mt-20 lbel25">
                                    <label>Shop Name*</label>
                                    <div className="ui left icon input swdh19">
                                        <input className="prompt srch_explore" placeholder="Buyer Name" type="text" disabled
                                        value={this.state.sellershowdata?.Shop?.name}/>															
                                    </div>
                                </div>
                                <div className="ui search focus mt-20 lbel25">
                                    <label>Shop Image*</label>
                                    <img src={this.state.sellershowdata?.Shop?.awsUrl} style={{width:100,height:100}}/>
                                </div>
                                <div className="ui search focus mt-20 lbel25">
                                    <label>Shop Address*</label>
                                    <div className="ui left icon input swdh19">
                                        <input className="prompt srch_explore" placeholder="Buyer Name" type="text" disabled
                                        value={this.state.sellershowdata?.Shop?.address}/>															
                                    </div>
                                </div>
                                <div className="ui search focus mt-20 lbel25">
                                    <label>Shop Description*</label>
                                    <div className="ui left icon input swdh19">
                                        <input className="prompt srch_explore" placeholder="Buyer Name" type="text" disabled
                                        value={this.state.sellershowdata?.Shop?.address}/>															
                                    </div>
                                </div>
                                <div className="ui search focus mt-20 lbel25">
                                    <label>Shop Type*</label>
                                    <div className="ui left icon input swdh19">
                                        <input className="prompt srch_explore" placeholder="Buyer Name" type="text" disabled
                                        value={this.state.sellershowdata?.Shop?.type}/>															
                                    </div>
                                </div>	
                                <div className="ui search focus mt-20 lbel25">
                                    <label>Shop Document Status*</label>
                                    <div className="ui left icon input swdh19">
                                        <input className="prompt srch_explore" placeholder="Buyer Name" type="text" disabled
                                        value={this.state.sellershowdata?.Shop?.documentStatus}/>															
                                    </div>
                                </div>										
                            </div>
                            <div className="col-md-12" style={{float:'left',paddingTop:20}}>
                                <h3 className="text-center" >Services</h3>
                                {(this.state.services.length==0)&&(
                                    <div className="col-md-12 text-center" style={{ padding:10 }}>
                                        <label><strong>No Services Added yet</strong> </label> 
                                    </div>
                                )}
                                {(this.state.services.map((item,index)=>(
                                    <div className="col-md-3 float-left" key={index} style={{ padding:10 }}>
                                        <div key={index} style={{backgroundColor:'#efefef',padding:10,borderRadius:10}}>
                                            <label><strong>Name:</strong> {item.ServiceCategory.name} </label><br></br>
                                            <label><strong>Brand:</strong> {item.Brand.name}</label><br></br>
                                            <label><strong>Model:</strong> {item.Model.name}</label><br></br>
                                            <label><strong>Description:</strong> {item.description} </label><br></br>
                                        </div>
                                    </div>
                                )))}
                            </div>
                            <div className="col-md-12" style={{float:'left',paddingTop:20}}>
                                <h3 className="text-center" >Shop Products</h3>
                                {(this.state.products.length==0)&&(
                                    <div className="col-md-12 text-center" style={{ padding:10 }}>
                                        <label><strong>No products Added yet</strong> </label> 
                                    </div>
                                )}
                                {(this.state.products.map((item,index)=>(
                                    <div className="col-md-3 float-left" key={index} style={{ padding:10 }}>
                                        <div key={index} style={{backgroundColor:'#efefef',padding:10,borderRadius:10}}>
                                            <img src={item.resources[0].awsUrl} style={{width:"100%",height:200,objectFit:'cover',marginBottom:10}}/>
                                            <label><strong>Name:</strong> {item.name} </label><br></br>
                                            <label><strong>Price:</strong> {item.price}</label><br></br>
                                            <label><strong>Type:</strong> {item.type}</label><br></br>
                                            <label><strong>Model:</strong> {item.Model.name}</label><br></br>
                                            <label><strong>Category:</strong> {item.Category.name}</label><br></br>
                                            <label><strong>Brand:</strong> {item.Brand.name}</label><br></br>
                                        </div>
                                    </div>
                                )))}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div className="modal fade" id="myPackageModal" role="dialog" >
                <div className="modal-dialog modal-lg" style={{maxWidth:1100}}>
                    <div className="modal-content">
                        <div className="modal-header">
                        <h3 className="text-center">Package Update</h3>
                        <button type="button" className="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div className="modal-body" style={{paddingBottom:30}}>
                            <div className="col-lg-12 col-md-12" style={{float:'left'}}>
                                <div className="ui search focus mt-20 lbel25">
                                    <label>Select Package*</label>
                                    <Select options={this.state.packagedata.map((item) => {
                                            return { value: item.id, label: item.name };
                                        })}
                                        onChange={entry => {
                                            console.log('====================================');
                                            console.log(entry);
                                            console.log('====================================');
                                            this.setState({packageid:entry.value})
                                        }}
                                        className="basic-multi-select"
                                        classNamePrefix="select"
                                    />
                                </div>	
                                <div className="col-lg-12 col-md-12" onClick={()=>{this.changepackage();}}>	
                                    <button className="discount_btn" data-dismiss="modal">Save</button>										
                                </div>									
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div className="modal fade" id="myChatModal" role="dialog" >
                <div className="modal-dialog modal-lg" style={{maxWidth:1100}}>
                    <div className="modal-content">
                        <div className="modal-header">
                            <h3 className="text-center">Chats</h3>
                            <button type="button" className="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div className="modal-body" style={{paddingBottom:30}}>
                            <div className="col-lg-12 col-md-12" style={{float:'left',cursor:'pointer'}}>
                                <label><strong>ALL Chats</strong></label><br></br>
                                {(this.state.chatdata.map((item,index)=>(
                                    <div className="col-md-3 float-left" key={index} style={{ padding:10 }} 
                                        onClick={()=>{
                                            this.setState({chatviewdata:item});
                                        }}
                                    >
                                        <div key={index} style={{backgroundColor:'#efefef',padding:10,borderRadius:10}}>
                                            <img src={item.User1.User.awsUrl} style={{width:"100%",height:200,objectFit:'cover',marginBottom:10}}/>
                                            <label><strong>Name:</strong> {item.User1.User.fullName} </label><br></br>
                                            <label><strong>Phone Number:</strong> {item.User1.User.phoneNumber}</label><br></br>
                                        </div>
                                    </div>
                                )))}									
                            </div>
                            <div className="col-lg-12 col-md-12" style={{float:'left'}}>
                                <label><strong>Chat View</strong></label><br></br>
                                {(this.state.chatviewdata?.Messages?.length==0)&&(
                                    <label>No Message to show</label>
                                )}
                                {(this.state.chatviewdata?.Messages?.map((item,index)=>(
                                    <div className="col-md-12 float-left" key={index} style={{ padding:10 }}>
                                        <div key={index} style={{backgroundColor:'#efefef',padding:10,borderRadius:10}}>
                                            <label>User : {item.userChatDetailId==this.state.chatviewdata.userChatDetailId1?this.state.chatviewdata.User1.User.fullName+' (Buyer)':this.state.chatviewdata.User2.User.fullName+' (Seller)'} </label><br></br>
                                            <label>{item.message.match('@video:')?item.message.replace('@video:', ''):item.message.match('@image:')?item.message.replace('@image:', ''):item.message.match('@location:')?'Google Maps Location':item.message} </label><br></br>
                                        </div>
                                    </div>
                                )))}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            
            <Footer/>
            </>
        )
    }
}
