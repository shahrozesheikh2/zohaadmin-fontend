import React, { Component } from 'react'
import Footer from '../components/Footer';
import Header from '../components/Header';
import Sidebar from '../components/Sidebar';
import axios from "axios";
export default class Package extends Component {
    constructor(props) {
        super(props);
        this.state = { token:localStorage.getItem('zohausertoken'),
            packagedata:[],
            viewpackage:'',
            editcode:'',
            editname:'',
            editphonecode:'',
            name:'',
            code:'',
            phonecode:'',
            editid:'',
            editprice:''
        };
        this.getpackage();
        
    }
    componentDidMount() {
    }
    getpackage=()=>{
        this.setState({editcode:'',
        editname:'',
        editphonecode:'',
        name:'',
        code:'',
        phonecode:''})
        const url = global.url+'/api/package?offset=0&limit=100';
        axios.get(url).then(response => {
            console.log('====================================');
            console.log(response.data.result.data);
            console.log('====================================');
            this.setState({packagedata:response.data.result.data})
        }).catch(err=>{
        console.log(err)
        });
    }
    viewpackage=(id)=>{
        var header={
            "Authorization":this.state.token
        }
        const url = global.url+'/api/package/'+id;
        axios.get(url,{headers:header}).then(response => {
            console.log('====================================');
            console.log(response.data.result.data);
            console.log('====================================');
            this.setState({viewpackage:response.data.result.data})
        }).catch(err=>{
        console.log(err)
        });
    }
    deletepackage=(id)=>{
        const url = global.url+'/api/package/'+id;
        var header={
            "Authorization":this.state.token
        }
        axios.delete(url,{headers:header}).then(response => {
            console.log('====================================');
            console.log(response.data.result.data);
            console.log('====================================');
            this.getpackage();
        }).catch(err=>{
        console.log(err)
        alert(err.response.data.message)
        });
    }
    updatepackage=()=>{
        var header={
            "Authorization":this.state.token
        }
        var body={
            "numberOfProducts": this.state.editcode,
            "price": this.state.editprice
        }
        const url = global.url+'/api/package/'+this.state.editid;
        axios.put(url,body,{headers:header}).then(response => {
            console.log('====================================');
            console.log(response.data);
            console.log('====================================');
            this.getpackage();
        }).catch(err=>{
        console.log(err)
        alert(err.response.data.message)
        });
    }
    addpackage=()=>{
        var header={
            "Authorization":this.state.token
        }
        var body={
            "name": this.state.name,
            "numberOfProducts": this.state.code
        }
        const url = global.url+'/api/package/';
        axios.post(url,body,{headers:header}).then(response => {
            console.log('====================================');
            console.log(response.data);
            console.log('====================================');
            this.getpackage();
        }).catch(err=>{
        console.log(err)
        alert(err.response.data.message)
        });
    }
    render() {
        return (
            <>
            <Header/>
            <Sidebar sidebarscreenname={'Package'}/>
            <div className="wrapper">
                <div className="sa4d25">
                    <div className="container-fluid">			
                        <div className="row">
                            <div className="col-lg-12">	
                                <h2 className="st_title"><i className="uil uil-play-circle"></i>Package</h2>
                            </div>	
                            <div className="col-lg-4">	
                            </div>
                            <div className="col-lg-4">	
                            </div>
                        </div>


                        {/* <div className="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                            <div className="panel panel-default">
                                <div className="panel-heading" role="tab" id="headingOne">
                                    <div className="panel-title adcrse1250">
                                        <a className="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                            Add Package
                                        </a>
                                    </div>
                                </div>
                                <div id="collapseOne" className="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                    <div className="panel-body adcrse_body">
                                        <div className="row">
                                            <div className="col-lg-12">
                                                <div className="discount_form">
                                                    <div className="row">
                                                        <div className="col-lg-12 col-md-12">	
                                                            <div className="ui search focus mt-20 lbel25">
                                                                <label>Package Name*</label>
                                                                <div className="ui left icon input swdh19">
                                                                    <input className="prompt srch_explore" placeholder="package Name" type="text" 
                                                                    value={this.state.name} onChange={(e) => {this.setState({name:e.target.value})}} required/>															
                                                                </div>
                                                            </div>										
                                                        </div>
                                                        <div className="col-lg-12 col-md-12">	
                                                            <div className="ui search focus mt-20 lbel25">
                                                                <label>Number Of Products*</label>
                                                                <div className="ui left icon input swdh19">
                                                                    <input className="prompt srch_explore" placeholder="package Name" type="text" 
                                                                    value={this.state.code} onChange={(e) => {this.setState({code:e.target.value})}} required/>															
                                                                </div>
                                                            </div>										
                                                        </div>
                                                        <div className="col-lg-12 col-md-12" onClick={()=>{this.addpackage()}}>	
                                                            <button className="discount_btn">Save</button>										
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                         */}
                        <div className="table-responsive mt-30">
                            <table className="table ucp-table">
                                <thead className="thead-s">
                                    <tr>
                                        {/* <th className="text-center" scope="col">Created Date</th> */}
                                        <th className="text-center" scope="col">Package Name</th>
                                        <th className="text-center" scope="col">Number Of Products</th>
                                        <th className="text-center" scope="col">Price</th>
                                        <th className="text-center" scope="col">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.state.packagedata.map((item,index)=>{
                                        return(
                                        <tr key={index}>    
                                            {/* <td className="text-center">{item.createdAt}</td> */}
                                            <td className="text-center">{item.name}</td>
                                            <td className="text-center">{item.numberOfProducts}</td>
                                            <td className="text-center">{item.price}</td>
                                            <td className="text-center">
                                                {/* <a href="javascript:void(0)" title="View" className="gray-s"><i className="uil uil-eye"></i></a> */}
                                                <a href="javascript:void(0)" title="Edit" className="gray-s" data-toggle="modal" data-target="#myModal" onClick={()=>{this.setState({
                                                    editid:item.id,
                                                    editcode:item.numberOfProducts,
                                                    editname:item.name,
                                                    editprice:item.price
                                                })}}><i className="uil uil-edit-alt"></i></a>
                                                {/* <a href="javascript:void(0)" title="Delete" className="gray-s" onClick={()=>{this.deletepackage(item.id)}}><i className="uil uil-trash-alt"></i></a> */}
                                            </td>
                                        </tr>
                                        )
                                    })}
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>


            <div className="modal fade" id="myModal" role="dialog" >
                <div className="modal-dialog modal-lg" style={{maxWidth:1100}}>
                    <div className="modal-content">
                        <div className="modal-header">
                        <button type="button" className="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div className="modal-body" style={{paddingBottom:30}}>
                            <div className="col-md-12">
                                <h3 className="text-center">Edit package</h3>
                            </div>
                            <div className="col-lg-12 col-md-12">	
                                <div className="ui search focus mt-20 lbel25">
                                    <label>Package Name*</label>
                                    <div className="ui left icon input swdh19">
                                        <input className="prompt srch_explore" placeholder="package Name" type="text" 
                                        value={this.state.editname} disabled required/>															
                                    </div>
                                </div>										
                            </div>
                            <div className="col-lg-12 col-md-12">	
                                <div className="ui search focus mt-20 lbel25">
                                    <label>Number Of Products*</label>
                                    <div className="ui left icon input swdh19">
                                        <input className="prompt srch_explore" placeholder="package Name" type="number" 
                                        value={this.state.editcode} onChange={(e) => {this.setState({editcode:e.target.value})}} required/>															
                                    </div>
                                </div>										
                            </div>
                            <div className="col-lg-12 col-md-12">	
                                <div className="ui search focus mt-20 lbel25">
                                    <label>Price*</label>
                                    <div className="ui left icon input swdh19">
                                        <input className="prompt srch_explore" placeholder="package Name" type="number" 
                                        value={this.state.editprice} onChange={(e) => {this.setState({editprice:e.target.value})}} required/>															
                                    </div>
                                </div>										
                            </div>
                            <div className="col-lg-12 col-md-12" onClick={()=>{this.updatepackage()}}>	
                                <button className="discount_btn" data-dismiss="modal">Save</button>										
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                
            <Footer/>
            </>
        )
    }
}
