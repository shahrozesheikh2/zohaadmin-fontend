import React, { Component } from 'react'
import Footer from '../components/Footer';
import Header from '../components/Header';
import Sidebar from '../components/Sidebar';
import axios from "axios";
import Select from 'react-select';
import ReactPaginate from 'react-paginate';
export default class City extends Component {
    constructor(props) {
        super(props);
        this.state = { token:localStorage.getItem('zohausertoken'),
            citydata:[],
            countrydata:[],
            viewcity:'',
            editcode:'',
            editname:'',
            editphonecode:'',
            name:'',
            code:'',
            phonecode:'',
            countryid:'',
            defaultcountryid:1,
            editid:'',
            totalpages:0,
            start:0,
            searchcity:''
        };
        this.getcity();
        this.getCountry();
    }
    componentDidMount() {
    }
    getCountry=()=>{
        
        const url = global.url+'/api/country?offset=0&limit=100';
        axios.get(url).then(response => {
            console.log('====================================');
            console.log(response.data.result.data);
            console.log('====================================');
            this.setState({countrydata:response.data.result.data})
        }).catch(err=>{
        console.log(err)
        });
    }
    getcity=()=>{
        this.setState({ editname:'',name:''})
        const url = global.url+'/api/city?offset='+this.state.start+'&limit=10&id='+this.state.defaultcountryid+'&cityName='+this.state.searchcity;
        axios.get(url).then(response => {
            console.log('====================================');
            console.log(response.data);
            console.log('====================================');
            this.setState({citydata:response.data.result.data});
            this.setState({totalpages:Math.ceil(response.data.result.totalCount/10)})
        }).catch(err=>{
        console.log(err)
        });
    }
    viewcity=(id)=>{
        var header={
            "Authorization":this.state.token
        }
        const url = global.url+'/api/city/'+id;
        axios.get(url,{headers:header}).then(response => {
            console.log('====================================');
            console.log(response.data.result.data);
            console.log('====================================');
            this.setState({viewcity:response.data.result.data})
        }).catch(err=>{
        console.log(err)
        });
    }
    deletecity=(id)=>{
        var header={
            "Authorization":this.state.token
        }
        const url = global.url+'/api/city/'+id;
        axios.delete(url,{headers:header}).then(response => {
            console.log('====================================');
            console.log(response.data.result.data);
            console.log('====================================');
            this.getcity();
        }).catch(err=>{
        console.log(err);
        alert(err.response.data.message)
        });
    }
    updatecity=()=>{
        var header={
            "Authorization":this.state.token
        }
        var body={
            "cityName": this.state.editname,
            "countryId": this.state.countryid
        }
        const url = global.url+'/api/city/'+this.state.editid;
        axios.put(url,body,{headers:header}).then(response => {
            console.log('====================================');
            console.log(response.data);
            console.log('====================================');
            this.getcity();
        }).catch(err=>{
        console.log(err);
        alert(err.response.data.message)
        });
    }
    addcity=()=>{
        var header={
            "Authorization":this.state.token
        }
        var body={
            "cityName": this.state.name,
            "countryId": this.state.countryid
        }
        const url = global.url+'/api/city/';
        axios.post(url,body,{headers:header}).then(response => {
            console.log('====================================');
            console.log(response.data);
            console.log('====================================');
            this.getcity();
        }).catch(err=>{
        console.log(err);
        alert(err.response.data.message)
        });
    }
    render() {
        return (
            <>
            <Header/>
            <Sidebar sidebarscreenname={'City'}/>
            <div className="wrapper">
                <div className="sa4d25">
                    <div className="container-fluid">			
                        <div className="row">
                            <div className="col-lg-4">	
                                <h2 className="st_title"><i className="uil uil-play-circle"></i>City</h2>
                            </div>	
                            <div className="col-lg-4">	
                            </div>
                            <div className="col-lg-4">	
                                <div className="ui search focus">
                                    <div className="ui left icon input swdh11 swdh15">
                                        <input className="prompt srch_explore" type="text" placeholder="City Name" value={this.state.searchcity} onChange={(e) => {this.setState({searchcity:e.target.value});
                                    setTimeout(() => {
                                        this.getcity();
                                    }, 100);}}/>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="col-lg-12 col-md-12">	
                            <div className="ui search focus mt-20 lbel25">
                                <h2 className="st_title">Select Country</h2>
                                <Select options={this.state.countrydata.map((item) => {
                                    return { value: item.id, label: item.countryName };
                                })}
                                onChange={entry => {
                                    console.log('====================================');
                                    console.log(entry);
                                    console.log('====================================');
                                    this.setState({ defaultcountryid: entry.value });
                                    setTimeout(() => {
                                        this.getcity();
                                    }, 100);
                                }}
                                defaultValue={{ label: "Pakistan", value: 1 }}
                                className="basic-multi-select"
                                classNamePrefix="select"
                                />
                            </div>										
                        </div>
                        <div className="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                            <div className="panel panel-default">
                                <div className="panel-heading" role="tab" id="headingOne">
                                    <div className="panel-title adcrse1250">
                                        <a className="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                            Add City
                                        </a>
                                    </div>
                                </div>
                                <div id="collapseOne" className="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                    <div className="panel-body adcrse_body">
                                        <div className="row">
                                            <div className="col-lg-12">
                                                <div className="discount_form">
                                                    <div className="row">
                                                        <div className="col-lg-12 col-md-12">	
                                                            <div className="ui search focus mt-20 lbel25">
                                                                <label>City Name*</label>
                                                                <div className="ui left icon input swdh19">
                                                                    <input className="prompt srch_explore" placeholder="city Name" type="text" 
                                                                    value={this.state.name} onChange={(e) => {this.setState({name:e.target.value})}} required/>															
                                                                </div>
                                                            </div>										
                                                        </div>
                                                        <div className="col-lg-12 col-md-12">	
                                                            <div className="ui search focus mt-20 lbel25">
                                                                <label>Country*</label>
                                                                <Select options={this.state.countrydata.map((item) => {
                                                                    return { value: item.id, label: item.countryName };
                                                                })}
                                                                onChange={entry => {
                                                                    console.log('====================================');
                                                                    console.log(entry);
                                                                    console.log('====================================');
                                                                    this.setState({ countryid: entry.value });
                                                                }}
                                                                // value={this.state.countryid}
                                                                className="basic-multi-select"
                                                                classNamePrefix="select"
                                                                />
                                                            </div>										
                                                        </div>
                                                        <div className="col-lg-12 col-md-12" onClick={()=>{this.addcity()}}>	
                                                            <button className="discount_btn" type="submit">Save</button>										
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div className="table-responsive mt-30">
                            <table className="table ucp-table">
                                <thead className="thead-s">
                                    <tr>
                                        <th className="text-center" scope="col">Created Date</th>
                                        <th className="text-center" scope="col">City Name</th>
                                        <th className="text-center" scope="col">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.state.citydata.map((item,index)=>{
                                        return(
                                        <tr key={index}>    
                                            <td className="text-center">{item.createdAt}</td>
                                            <td className="text-center">{item.cityName}</td>
                                            <td className="text-center">
                                                <a href="javascript:void(0)" title="Edit" className="gray-s"  data-toggle="modal" data-target="#myModal" onClick={()=>{this.setState({
                                                    editid:item.id,
                                                    editname:item.cityName
                                                })}}><i className="uil uil-edit-alt"></i></a>
                                                {/* <a href="javascript:void(0)" title="Delete" className="gray-s" onClick={()=>{this.deletecity(item.id)}}><i className="uil uil-trash-alt"></i></a> */}
                                            </td>
                                        </tr>
                                        )
                                    })}
                                    
                                </tbody>
                            </table>
                            <ReactPaginate
                                previousLabel={'PRE'}
                                nextLabel={'NEXT'}
                                breakLabel={'...'}
                                breakClassName={'break-me'}
                                pageCount={this.state.totalpages}
                                marginPagesDisplayed={2}
                                pageRangeDisplayed={5}
                                onPageChange={(data)=>{
                                console.log(data);
                                this.setState({start:data.selected});
                                setTimeout(() => {
                                    this.getcity();
                                }, 200);
                                }}
                                containerClassName={'pagination'}
                                subContainerClassName={'pages pagination'}
                                activeClassName={'active'}
                            />
                        </div>
                    </div>
                </div>
            </div>


            <div className="modal fade" id="myModal" role="dialog" >
                <div className="modal-dialog modal-lg" style={{maxWidth:1100}}>
                    <div className="modal-content">
                        <div className="modal-header">
                        <button type="button" className="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div className="modal-body" style={{paddingBottom:30}}>
                            <div className="col-md-12">
                                <h3 className="text-center">Edit City</h3>
                            </div>
                            <div className="col-lg-12 col-md-12">	
                                <div className="ui search focus mt-20 lbel25">
                                    <label>City Name*</label>
                                    <div className="ui left icon input swdh19">
                                        <input className="prompt srch_explore" placeholder="city Name" type="text" 
                                        value={this.state.editname} onChange={(e) => {this.setState({editname:e.target.value})}} required/>															
                                    </div>
                                </div>										
                            </div>
                            <div className="col-lg-12 col-md-12">	
                                <div className="ui search focus mt-20 lbel25">
                                    <label>Country*</label>
                                    <Select options={this.state.countrydata.map((item) => {
                                        return { value: item.id, label: item.countryName };
                                    })}
                                    onChange={entry => {
                                        console.log('====================================');
                                        console.log(entry);
                                        console.log('====================================');
                                        this.setState({ countryid: entry.value });
                                    }}
                                    // value={this.state.countryid}
                                    className="basic-multi-select"
                                    classNamePrefix="select"
                                    />
                                </div>										
                            </div>
                            <div className="col-lg-12 col-md-12" onClick={()=>{this.updatecity()}}>	
                                <button className="discount_btn"  data-dismiss="modal">Save</button>										
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <Footer/>
            </>
        )
    }
}
