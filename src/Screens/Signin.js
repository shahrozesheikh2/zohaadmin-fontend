import React, { Component } from 'react';
import { Link } from "react-router-dom";
import axios from "axios";
export default class Signin extends Component {
    constructor(props) {
        super(props);
        this.state = { token:localStorage.getItem('zohausertoken'), 
            email:'' , password:''
        };
        if(localStorage.getItem('zohausertoken')!=null){
            this.props.history.push('/dashboard');
        }
    }  
    loginfun(){
        var body={
            "email":this.state.email,
            "password":this.state.password
        }
        axios.post(global.url+'/api/admin/login', body ).then(res => {
            console.log(res.data.result.data.token);
            global.token=res.data.result.data.token;
            localStorage.setItem('zohausertoken', res.data.result.data.token);
            localStorage.setItem('zohauserrole', res.data.result.data.roleId);
            this.props.history.push('/dashboard');
        }).catch(err=>{
            console.log('====================================');
            console.log(err.response.data.message);
            console.log('====================================');
            alert(err.response.data.message)
        })
    }
  render() {
    return (
        <>
        <div className="sign_in_up_bg">
            <div className="container">
                <div className="row justify-content-lg-center justify-content-md-center">
                    <div className="col-lg-12">
                        <div className="main_logo25" id="logo">
                            <a href="index.html"><img src="images/logo.svg" alt=""/></a>
                            <a href="index.html"><img className="logo-inverse" src="images/ct_logo.svg" alt=""/></a>
                        </div>
                    </div>
                
                    <div className="col-lg-6 col-md-8">
                        <div className="sign_form">
                            <h2>Welcome Back</h2>
                            <p>Log In to Your Account!</p>
                            <div className="ui search focus mt-15">
                                <div className="ui left icon input swdh95">
                                    <input className="prompt srch_explore" type="email" name="emailaddress" value="" id="id_email" 
                                    value={this.state.email} onChange={(e) => {this.setState({email:e.target.value})}} 
                                    required="" placeholder="Email Address" />															
                                    <i className="uil uil-envelope icon icon2"></i>
                                </div>
                            </div>
                            <div className="ui search focus mt-15">
                                <div className="ui left icon input swdh95">
                                    <input className="prompt srch_explore" type="password" name="password"
                                    value={this.state.password} onChange={(e) => {this.setState({password:e.target.value})}} 
                                    id="id_password" required="" placeholder="Password" />
                                    <i className="uil uil-key-skeleton-alt icon icon2"></i>
                                </div>
                            </div>
                            <button className="login-btn" onClick={() => this.loginfun()} >
                                <span className="menu--label" style={{color:'white'}}>Sign In</span>
                            </button>
                        </div>
                        <div className="sign_footer"><img src="images/sign_logo.png" alt="" />© 2021 <strong>Zoha</strong>. All Rights Reserved.</div>
                    </div>				
                </div>				
            </div>				
        </div>
        </>
    );
  }
}
