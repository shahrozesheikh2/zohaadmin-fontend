import React, { Component } from 'react'
import Footer from '../components/Footer';
import Header from '../components/Header';
import Sidebar from '../components/Sidebar'; 
import axios from "axios";
import ReactPaginate from 'react-paginate';

export default class Otp extends Component {
    constructor(props) {
        super(props);
        this.state = { token:localStorage.getItem('zohausertoken'),
            otpdata:[],
            totalpages:0,
            start:0,
            searchname:'',
            phoneNumber:''
        };
        this.getbuyer();
    }
    getbuyer=()=>{
        var header={
            "Authorization":this.state.token
        }
        const url = global.url+'/api/admin/otp?offset='+this.state.start+'&limit=10&fullName='+this.state.searchname+'&phoneNumber='+this.state.phoneNumber.toString();
        axios.get(url,{headers:header}).then(response => {
            console.log('====================================');
            console.log(response);
            console.log('====================================');
            setTimeout(() => {
                this.getbuyer();
            }, 60000);
            this.setState({otpdata:response.data.result.data})
            this.setState({totalpages:Math.ceil(response.data.result.totalCount/10)})
        }).catch(err=>{
            console.log(err)
        });
    }
    render() {
        return (
            <>
            <Header/>
            <Sidebar sidebarscreenname={'Otp'}/>
            <div className="wrapper">
                <div className="sa4d25">
                    <div className="container-fluid">			
                        <div className="row">
                            <div className="col-lg-3">	
                                <h2 className="st_title"><i className="uil uil-play-circle"></i>OTP</h2>
                            </div>
                            <div className="col-lg-3">	</div>	
                            <div className="col-lg-3">	
                                <div className="ui search focus">
                                    <div className="ui left icon input swdh11 swdh15">
                                        <input className="prompt srch_explore" type="text" placeholder="Name" value={this.state.searchname} onChange={(e) => {this.setState({searchname:e.target.value});
                                    setTimeout(() => {
                                        this.getbuyer();
                                    }, 100);}}/>
                                        
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-3">	
                                <div className="ui search focus">
                                    <div className="ui left icon input swdh11 swdh15">
                                        <input className="prompt srch_explore" type="text" placeholder="Phone" value={this.state.phoneNumber} onChange={(e) => {this.setState({phoneNumber:e.target.value});
                                    setTimeout(() => {
                                        this.getbuyer();
                                    }, 100);}}/>
                                        
                                    </div>
                                </div>
                            </div>	
                        </div>
                        <div className="table-responsive mt-30">
                            <table className="table ucp-table">
                                <thead className="thead-s">
                                    <tr>
                                        {/* <th className="text-center" scope="col">Created Date</th> */}
                                        <th className="text-center" scope="col">User Name</th>
                                        <th className="text-center" scope="col">Phone</th>
                                        <th className="text-center" scope="col">Code</th>
                                    </tr>
                                </thead>
                                <tbody>
                                {this.state.otpdata.map((item,index)=>{
                                        return(
                                    <tr key={index}>
                                        {/* <td className="text-center">{item.createdAt}</td> */}
                                        <td className="text-center">{item.User.fullName}</td>
                                        <td className="text-center">{item.User.phoneNumber}</td>
                                        <td className="text-center">{item.code}</td>
                                    </tr>
                                    )
                                })}
                                </tbody>
                            </table>
                        </div>
                        <ReactPaginate
                                previousLabel={'PRE'}
                                nextLabel={'NEXT'}
                                breakLabel={'...'}
                                breakClassName={'break-me'}
                                pageCount={this.state.totalpages}
                                marginPagesDisplayed={2}
                                pageRangeDisplayed={5}
                                onPageChange={(data)=>{
                                console.log(data);
                                this.setState({start:data.selected});
                                setTimeout(() => {
                                    this.getbuyer();
                                }, 200);
                                }}
                                containerClassName={'pagination'}
                                subContainerClassName={'pages pagination'}
                                activeClassName={'active'}
                            />
                        
                    </div>
                </div>
            </div>

            <Footer/>
            </>
        )
    }
}
