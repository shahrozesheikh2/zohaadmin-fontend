import React, { Component } from 'react'
import Footer from '../components/Footer';
import Header from '../components/Header';
import Sidebar from '../components/Sidebar';
export default class Importer extends Component {
    render() {
        return (
            <>
            <Header/>
            <Sidebar sidebarscreenname={'Importer'}/>
            <div className="wrapper">
                <div className="sa4d25">
                    <div className="container-fluid">			
                        <div className="row">
                            <div className="col-lg-12">	
                                <h2 className="st_title"><i className="uil uil-play-circle"></i>Importer</h2>
                            </div>	
                            <div className="col-lg-4">	
                            </div>
                            <div className="col-lg-4">	
                            </div>
                        </div>
                        <div className="table-responsive mt-30">
                            <table className="table ucp-table">
                                <thead className="thead-s">
                                    <tr>
                                        <th className="text-center" scope="col"> Date Posted</th>
                                        <th className="text-center" scope="col">Name</th>
                                        <th className="text-center" scope="col">Phone</th>
                                        <th className="text-center" scope="col">City</th>
                                        <th className="text-center" scope="col">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td className="text-center">02 Nov 2019 12:00 PM</td>
                                        <td className="text-center">Importer</td>
                                        <td className="text-center">+923054234321</td>
                                        <td className="text-center">Islamabad</td>
                                        <td className="text-center">
                                            
                                            <a href="javascript:void(0)" title="View" className="gray-s"><i className="uil uil-eye"></i></a>
                                            
                                        </td>
                                    </tr>
                                    <tr>
                                        <td className="text-center">02 Nov 2019 12:00 PM</td>
                                        <td className="text-center">Importer</td>
                                        <td className="text-center">+923054234321</td>
                                        <td className="text-center">Islamabad</td>
                                        <td className="text-center">
                                            
                                            <a href="javascript:void(0)" title="Edit" className="gray-s"><i className="uil uil-eye"></i></a>
                                            
                                        </td>
                                    </tr>
                                    <tr>
                                        <td className="text-center">02 Nov 2019 12:00 PM</td>
                                        <td className="text-center">Importer</td>
                                        <td className="text-center">+923054234321</td>
                                        <td className="text-center">Islamabad</td>
                                        <td className="text-center">
                                            
                                            <a href="javascript:void(0)" title="Edit" className="gray-s"><i className="uil uil-eye"></i></a>
                                            
                                        </td>
                                    </tr>
                                    <tr>
                                        <td className="text-center">02 Nov 2019 12:00 PM</td>
                                        <td className="text-center">Importer</td>
                                        <td className="text-center">+923054234321</td>
                                        <td className="text-center">Islamabad</td>
                                        <td className="text-center">
                                            
                                            <a href="javascript:void(0)" title="Edit" className="gray-s"><i className="uil uil-eye"></i></a>
                                            
                                        </td>
                                    </tr>
                                    <tr>
                                        <td className="text-center">02 Nov 2019 12:00 PM</td>
                                        <td className="text-center">Importer</td>
                                        <td className="text-center">+923054234321</td>
                                        <td className="text-center">Islamabad</td>
                                        <td className="text-center">
                                            
                                            <a href="javascript:void(0)" title="Edit" className="gray-s"><i className="uil uil-eye"></i></a>
                                            
                                        </td>
                                    </tr>
                                    <tr>
                                        <td className="text-center">02 Nov 2019 12:00 PM</td>
                                        <td className="text-center">Importer</td>
                                        <td className="text-center">+923054234321</td>
                                        <td className="text-center">Islamabad</td>
                                        <td className="text-center">
                                            
                                            <a href="javascript:void(0)" title="Edit" className="gray-s"><i className="uil uil-eye"></i></a>
                                            
                                        </td>
                                    </tr>
                                    <tr>
                                        <td className="text-center">02 Nov 2019 12:00 PM</td>
                                        <td className="text-center">Importer</td>
                                        <td className="text-center">+923054234321</td>
                                        <td className="text-center">Islamabad</td>
                                        <td className="text-center">
                                            
                                            <a href="javascript:void(0)" title="Edit" className="gray-s"><i className="uil uil-eye"></i></a>
                                            
                                        </td>
                                    </tr>
                                    <tr>
                                        <td className="text-center">02 Nov 2019 12:00 PM</td>
                                        <td className="text-center">Importer</td>
                                        <td className="text-center">+923054234321</td>
                                        <td className="text-center">Islamabad</td>
                                        <td className="text-center">
                                            
                                            <a href="javascript:void(0)" title="Edit" className="gray-s"><i className="uil uil-eye"></i></a>
                                            
                                        </td>
                                    </tr>
                                    <tr>
                                        <td className="text-center">02 Nov 2019 12:00 PM</td>
                                        <td className="text-center">Importer</td>
                                        <td className="text-center">+923054234321</td>
                                        <td className="text-center">Islamabad</td>
                                        <td className="text-center">
                                            
                                            <a href="javascript:void(0)" title="Edit" className="gray-s"><i className="uil uil-eye"></i></a>
                                            
                                        </td>
                                    </tr>
                                    <tr>
                                        <td className="text-center">02 Nov 2019 12:00 PM</td>
                                        <td className="text-center">Importer</td>
                                        <td className="text-center">+923054234321</td>
                                        <td className="text-center">Islamabad</td>
                                        <td className="text-center">
                                            
                                            <a href="javascript:void(0)" title="Edit" className="gray-s"><i className="uil uil-eye"></i></a>
                                            
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        
                    </div>
                </div>
            </div>
            <Footer/>
            </>
        )
    }
}
