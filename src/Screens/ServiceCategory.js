import React, { Component } from 'react'
import Footer from '../components/Footer';
import Header from '../components/Header';
import Sidebar from '../components/Sidebar';
import axios from "axios";

export default class ServiceCategory extends Component {
    constructor(props) {
        super(props);
        this.state = { token:localStorage.getItem('zohausertoken'),
            categorydata:[],
            viewcategory:'',
            editcode:'',
            editname:'',
            editphonecode:'',
            name:'',
            code:'',
            phonecode:'',
            editid:''
        };
        this.getcategory();
        
    }
    componentDidMount() {
    }
    getcategory=()=>{
        this.setState({ editname:'',name:''})
        const url = global.url+'/api/service-category?offset=0&limit=200';
        axios.get(url).then(response => {
            console.log('====================================');
            console.log(response.data.result.data);
            console.log('====================================');
            this.setState({categorydata:response.data.result.data})
        }).catch(err=>{
        console.log(err)
        });
    }
    viewcategory=(id)=>{
        const url = global.url+'/api/service-category/'+id;
        axios.get(url).then(response => {
            console.log('====================================');
            console.log(response.data.result.data);
            console.log('====================================');
            this.setState({viewcategory:response.data.result.data})
        }).catch(err=>{
        console.log(err)
        });
    }
    deletecategory=(id)=>{
        var header={
            "Authorization":this.state.token
        }
        const url = global.url+'/api/service-category/'+id;
        axios.delete(url,{headers:header}).then(response => {
            console.log('====================================');
            console.log(response.data.result.data);
            console.log('====================================');
            this.getcategory();
        }).catch(err=>{
        console.log(err);
        alert(err.response.data.message)
        });
    }
    updatecategory=()=>{
        var header={
            "Authorization":this.state.token
        }
        var body={
            "name": this.state.editname
        }
        const url = global.url+'/api/service-category/'+this.state.editid;
        axios.put(url,body,{headers:header}).then(response => {
            console.log('====================================');
            console.log(response.data);
            console.log('====================================');
            this.getcategory();
        }).catch(err=>{
        console.log(err);
        alert(err.response.data.message)
        });
    }
    addcategory=()=>{
        var header={
            "Authorization":this.state.token
        }
        var body={
            "name": this.state.name
        }
        const url = global.url+'/api/service-category/';
        axios.post(url,body,{headers:header}).then(response => {
            console.log('====================================');
            console.log(response.data);
            console.log('====================================');
            this.getcategory();
        }).catch(err=>{
        console.log(err);
        alert(err.response.data.message)
        });
    }
    render() {
        return (
            <>
            <Header/>
            <Sidebar sidebarscreenname={'ServiceCategory'}/>
            <div className="wrapper">
                <div className="sa4d25">
                    <div className="container-fluid">			
                        <div className="row">
                            <div className="col-lg-12">	
                                <h2 className="st_title"><i className="uil uil-play-circle"></i>Service Category</h2>
                            </div>	
                            <div className="col-lg-4">	
                            </div>
                            <div className="col-lg-4">	
                            </div>
                        </div>


                        <div className="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                            <div className="panel panel-default">
                                <div className="panel-heading" role="tab" id="headingOne">
                                    <div className="panel-title adcrse1250">
                                        <a className="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                            Add Service Category
                                        </a>
                                    </div>
                                </div>
                                <div id="collapseOne" className="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                    <div className="panel-body adcrse_body">
                                        <div className="row">
                                            <div className="col-lg-12">
                                                <div className="discount_form">
                                                    <div className="row">
                                                        <div className="col-lg-12 col-md-12">	
                                                            <div className="ui search focus mt-20 lbel25">
                                                                <label>Service Category Name*</label>
                                                                <div className="ui left icon input swdh19">
                                                                    <input className="prompt srch_explore" placeholder="Service Category Name" type="text" 
                                                                    value={this.state.name} onChange={(e) => {this.setState({name:e.target.value})}} required/>															
                                                                </div>
                                                            </div>										
                                                        </div>
                                                        <div className="col-lg-12 col-md-12" onClick={()=>{this.addcategory()}}>	
                                                            <button className="discount_btn" type="submit">Save</button>										
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="table-responsive mt-30">
                            <table className="table ucp-table">
                                <thead className="thead-s">
                                    <tr>
                                        <th className="text-center" scope="col">#</th>
                                        <th className="text-center" scope="col">Service Category Name</th>
                                        <th className="text-center" scope="col">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.state.categorydata.map((item,index)=>{
                                        return(
                                        <tr key={index}>    
                                            <td className="text-center">{index+1}</td>
                                            <td className="text-center">{item.name}</td>
                                            <td className="text-center">
                                                <a href="javascript:void(0)" title="Edit" className="gray-s" data-toggle="modal" data-target="#myModal" onClick={()=>{this.setState({
                                                    editid:item.id,
                                                    editname:item.name
                                                })}}><i className="uil uil-edit-alt"></i></a>
                                                {/* <a href="javascript:void(0)" title="Delete" className="gray-s" onClick={()=>{this.deletecategory(item.id)}}><i className="uil uil-trash-alt"></i></a> */}
                                            </td>
                                        </tr>
                                        )
                                    })}
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>





            <div className="modal fade" id="myModal" role="dialog" >
                <div className="modal-dialog modal-lg" style={{maxWidth:1100}}>
                    <div className="modal-content">
                        <div className="modal-header">
                        <button type="button" className="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div className="modal-body" style={{paddingBottom:30}}>
                            <div className="col-md-12">
                                <h3 className="text-center">Edit Service Category</h3>
                            </div>
                            <div className="col-lg-12 col-md-12">	
                                <div className="ui search focus mt-20 lbel25">
                                    <label>Service Category Name*</label>
                                    <div className="ui left icon input swdh19">
                                        <input className="prompt srch_explore" placeholder="Service Category Name" type="text" 
                                        value={this.state.editname} onChange={(e) => {this.setState({editname:e.target.value})}} required/>															
                                    </div>
                                </div>										
                            </div>
                            <div className="col-lg-12 col-md-12" onClick={()=>{this.updatecategory()}}>	
                                <button className="discount_btn"  data-dismiss="modal">Save</button>										
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <Footer/>
            </>
        )
    }
}
