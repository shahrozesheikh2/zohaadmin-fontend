import React, { Component } from 'react'
import Footer from '../components/Footer';
import Header from '../components/Header';
import Sidebar from '../components/Sidebar';
import axios from "axios";
export default class Country extends Component {
    constructor(props) {
        super(props);
        this.state = { token:localStorage.getItem('zohausertoken'),
            countrydata:[],
            viewcountry:'',
            editcode:'',
            editname:'',
            editphonecode:'',
            name:'',
            code:'',
            phonecode:'',
            editid:''
        };
        this.getCountry();
        
    }
    componentDidMount() {
    }
    getCountry=()=>{
        this.setState({editcode:'',
        editname:'',
        editphonecode:'',
        name:'',
        code:'',
        phonecode:''})
        const url = global.url+'/api/country?offset=0&limit=100';
        axios.get(url).then(response => {
            console.log('====================================');
            console.log(response.data.result.data);
            console.log('====================================');
            this.setState({countrydata:response.data.result.data})
        }).catch(err=>{
        console.log(err);
        });
    }
    viewCountry=(id)=>{
        var header={
            "Authorization":this.state.token
        }
        const url = global.url+'/api/country/'+id;
        axios.get(url,{headers:header}).then(response => {
            console.log('====================================');
            console.log(response.data.result.data);
            console.log('====================================');
            this.setState({viewcountry:response.data.result.data})
        }).catch(err=>{
        console.log(err);
        });
    }
    deleteCountry=(id)=>{
        const url = global.url+'/api/country/'+id;
        var header={
            "Authorization":this.state.token
        }
        axios.delete(url,{headers:header}).then(response => {
            console.log('====================================');
            console.log(response.data.result.data);
            console.log('====================================');
            this.getCountry();
        }).catch(err=>{
        console.log(err);
        alert(err.response.data.message)
        });
    }
    updateCountry=(id)=>{
        var header={
            "Authorization":this.state.token
        }
        var body={
            "countryName": this.state.editname,
            "countryCode": this.state.editcode,
            "phoneCode": this.state.editphonecode
        }
        const url = global.url+'/api/country/'+this.state.editid;
        axios.put(url,body,{headers:header}).then(response => {
            console.log('====================================');
            console.log(response.data);
            console.log('====================================');
            this.getCountry();
        }).catch(err=>{
        console.log(err);
        alert(err.response.data.message)
        });
    }
    addCountry=()=>{
        var header={
            "Authorization":this.state.token
        }
        var body={
            "countryName": this.state.name,
            "countryCode": this.state.code,
            "phoneCode": this.state.phonecode
        }
        const url = global.url+'/api/country/';
        axios.post(url,body,{headers:header}).then(response => {
            console.log('====================================');
            console.log(response.data);
            console.log('====================================');
            this.getCountry();
        }).catch(err=>{
        console.log(err);
        alert(err.response.data.message)
        });
    }
    render() {
        return (
            <>
            <Header/>
            <Sidebar sidebarscreenname={'Country'}/>
            <div className="wrapper">
                <div className="sa4d25">
                    <div className="container-fluid">			
                        <div className="row">
                            <div className="col-lg-12">	
                                <h2 className="st_title"><i className="uil uil-play-circle"></i>Country</h2>
                            </div>	
                            <div className="col-lg-4">	
                            </div>
                            <div className="col-lg-4">	
                            </div>
                        </div>


                        <div className="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                            <div className="panel panel-default">
                                <div className="panel-heading" role="tab" id="headingOne">
                                    <div className="panel-title adcrse1250">
                                        <a className="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                            Add Country
                                        </a>
                                    </div>
                                </div>
                                <div id="collapseOne" className="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                    <div className="panel-body adcrse_body">
                                        <div className="row">
                                            <div className="col-lg-12">
                                                <div className="discount_form">
                                                    <div className="row">
                                                        <div className="col-lg-12 col-md-12">	
                                                            <div className="ui search focus mt-20 lbel25">
                                                                <label>Country Name*</label>
                                                                <div className="ui left icon input swdh19">
                                                                    <input className="prompt srch_explore" placeholder="Country Name" type="text" 
                                                                    value={this.state.name} onChange={(e) => {this.setState({name:e.target.value})}} required/>															
                                                                </div>
                                                            </div>										
                                                        </div>
                                                        <div className="col-lg-12 col-md-12">	
                                                            <div className="ui search focus mt-20 lbel25">
                                                                <label>Country Code*</label>
                                                                <div className="ui left icon input swdh19">
                                                                    <input className="prompt srch_explore" placeholder="Country Code" type="text" 
                                                                    value={this.state.code} onChange={(e) => {this.setState({code:e.target.value})}} required/>															
                                                                </div>
                                                            </div>										
                                                        </div>
                                                        <div className="col-lg-12 col-md-12">	
                                                            <div className="ui search focus mt-20 lbel25">
                                                                <label>Phone Code*</label>
                                                                <div className="ui left icon input swdh19">
                                                                    <input className="prompt srch_explore" placeholder="Phone Code" type="text" 
                                                                    value={this.state.phonecode} onChange={(e) => {this.setState({phonecode:e.target.value})}} required/>															
                                                                </div>
                                                            </div>										
                                                        </div>
                                                        <div className="col-lg-12 col-md-12" onClick={()=>{this.addCountry()}}>	
                                                            <button className="discount_btn">Save</button>										
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="table-responsive mt-30">
                            <table className="table ucp-table">
                                <thead className="thead-s">
                                    <tr>
                                        <th className="text-center" scope="col">#</th>
                                        <th className="text-center" scope="col">Country Name</th>
                                        <th className="text-center" scope="col">Country Code</th>
                                        <th className="text-center" scope="col">Phone Code</th>
                                        <th className="text-center" scope="col">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.state.countrydata.map((item,index)=>{
                                        return(
                                        <tr key={index}>    
                                            <td className="text-center">{index+1}</td>
                                            <td className="text-center">{item.countryName}</td>
                                            <td className="text-center">{item.countryCode}</td>
                                            <td className="text-center">{item.phoneCode}</td>
                                            <td className="text-center">
                                                {/* <a href="javascript:void(0)" title="View" className="gray-s"><i className="uil uil-eye"></i></a> */}
                                                <a href="javascript:void(0)" title="Edit" className="gray-s" data-toggle="modal" data-target="#myModal" onClick={()=>{this.setState({
                                                    editid:item.id,
                                                    editcode:item.countryCode,
                                                    editname:item.countryName,
                                                    editphonecode:item.phoneCode
                                                })}}><i className="uil uil-edit-alt"></i></a>
                                                {/* <a href="javascript:void(0)" title="Delete" className="gray-s" onClick={()=>{this.deleteCountry(item.id)}}><i className="uil uil-trash-alt"></i></a> */}
                                            </td>
                                        </tr>
                                        )
                                    })}
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>


            <div className="modal fade" id="myModal" role="dialog" >
                <div className="modal-dialog modal-lg" style={{maxWidth:1100}}>
                    <div className="modal-content">
                        <div className="modal-header">
                        <button type="button" className="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div className="modal-body" style={{paddingBottom:30}}>
                            <div className="col-md-12">
                                <h3 className="text-center">Edit Country</h3>
                            </div>
                            <div className="col-lg-12 col-md-12">	
                                <div className="ui search focus mt-20 lbel25">
                                    <label>Country Name*</label>
                                    <div className="ui left icon input swdh19">
                                        <input className="prompt srch_explore" placeholder="Country Name" type="text" 
                                        value={this.state.editname} onChange={(e) => {this.setState({editname:e.target.value})}} required/>															
                                    </div>
                                </div>										
                            </div>
                            <div className="col-lg-12 col-md-12">	
                                <div className="ui search focus mt-20 lbel25">
                                    <label>Country Code*</label>
                                    <div className="ui left icon input swdh19">
                                        <input className="prompt srch_explore" placeholder="Country Code" type="text" 
                                        value={this.state.editcode} onChange={(e) => {this.setState({editcode:e.target.value})}} required/>															
                                    </div>
                                </div>										
                            </div>
                            <div className="col-lg-12 col-md-12">	
                                <div className="ui search focus mt-20 lbel25">
                                    <label>Phone Code*</label>
                                    <div className="ui left icon input swdh19">
                                        <input className="prompt srch_explore" placeholder="Phone Code" type="text" 
                                        value={this.state.editphonecode} onChange={(e) => {this.setState({editphonecode:e.target.value})}} required/>															
                                    </div>
                                </div>										
                            </div>
                            <div className="col-lg-12 col-md-12" onClick={()=>{this.updateCountry()}}>	
                                <button className="discount_btn" data-dismiss="modal">Save</button>										
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                
            <Footer/>
            </>
        )
    }
}
