import React, { Component } from 'react';
import Header from '../components/Header';
import Sidebar from '../components/Sidebar';
import Footer from '../components/Footer';
import axios from "axios";
import { Line } from 'react-chartjs-2';
export default class Dashboard extends Component {
	constructor(props){
		super(props);
		this.state={
			token:localStorage.getItem('zohausertoken'),
			dashboarddata:'',
			DashboardGraph:{
				labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul","Aug","Sep","Oct","Nov","Dec"],
				datasets: [
				  {
					label: "Seller",
					pointRadius: 4,
					pointBackgroundColor: "rgba(255,255,255,1)",
					pointBorderWidth: 2,
					fill: true,
					lineTension: 0,
					backgroundColor: "rgba(66,208,163,0.2)",
					borderWidth: 2.5,
					borderColor: "#42d0a3",
					data: []
				  },
				  {
					label: "Buyer",
					pointRadius: 4,
					pointBackgroundColor: "rgba(255,255,255,1)",
					pointBorderWidth: 2,
					fill: true,
					lineTension: 0,
					backgroundColor: "rgba(76,132,255,0.2)",
					borderWidth: 2.5,
					borderColor: "#4c84ff",
					data: []
				  }
				]
			}

		}
		this.getDashboard()
	}
	getDashboard=()=>{
		var localLine;
		var header={
            "Authorization":this.state.token
        }
        const url = global.url+'/api/admin/dashboard';
        axios.get(url,{headers:header}).then(response => {
            console.log('====================================');
            console.log(response.data.result.data);
            console.log('====================================');
            this.setState({dashboarddata:response.data.result.data})
			this.setState({dashboarddata:response.data.result.data})
			localLine={
				labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul","Aug","Sep","Oct","Nov","Dec"],
				datasets: [
				  {
					label: "Seller",
					pointRadius: 4,
					pointBackgroundColor: "rgba(255,255,255,1)",
					pointBorderWidth: 2,
					fill: true,
					lineTension: 0,
					backgroundColor: "rgba(66,208,163,0.2)",
					borderWidth: 2.5,
					borderColor: "#42d0a3",
					data: response.data.result.data.sellerCountByMonth
				  },
				  {
					label: "Buyer",
					pointRadius: 4,
					pointBackgroundColor: "rgba(255,255,255,1)",
					pointBorderWidth: 2,
					fill: true,
					lineTension: 0,
					backgroundColor: "rgba(76,132,255,0.2)",
					borderWidth: 2.5,
					borderColor: "#4c84ff",
					data: response.data.result.data.buyerCountByMonth
				  }
				]
			}
			// localLine.datasets[0].data.push(response.data.result.data.sellerCountByMonth);
			// localLine.datasets[1].data.push(response.data.result.data.buyerCountByMonth);
			console.log('==========localLine==========================');
			console.log(localLine);
			console.log('==========localLine==========================');
			this.setState({DashboardGraph:localLine})
        }).catch(err=>{
        console.log(err)
        });
    }
  render() {
    return (
        <>
        <Header/>
        <Sidebar sidebarscreenname={'Dashboard'}/>
        <div className="wrapper">
		<div className="sa4d25">
			<div className="container-fluid">			
				<div className="row">
					<div className="col-lg-12">	
						<h2 className="st_title"><i className="uil uil-apps"></i>Super Admin Dashboard</h2>
					</div>
					<div className="col-xl-6 col-lg-6 col-md-6">
						<div className="card_dash">
							<div className="card_dash_left">
								<h5 style={{marginBottom:10}}>Number of Buyers</h5>
								<h2>{this.state.dashboarddata.buyerCount}</h2>
							</div>
							<div className="card_dash_right p-0">
								<img src="./images/dashboard/menwomen.png" alt="" style={{width:100}}/>
							</div>
						</div>
					</div>
					<div className="col-xl-6 col-lg-6 col-md-6">
						<div className="card_dash">
							<div className="card_dash_left">
								<h5 style={{marginBottom:10}}>Number of Shops</h5>
								<h2>{this.state.dashboarddata.sellerCount}</h2>
							</div>
							<div className="card_dash_right p-0">
								<img src="./images/dashboard/age13-18.png" alt="" style={{width:100}}/>
							</div>
						</div>
					</div>
					{/* <div className="col-xl-4 col-lg-6 col-md-6">
						<div className="card_dash">
							<div className="card_dash_left">
								<h5 style={{marginBottom:10}}>Number of Wholesaler</h5>
								<h2>1500</h2>
							</div>
							<div className="card_dash_right p-0">
								<img src="./images/dashboard/age13-18.png" alt="" style={{width:100}}/>
							</div>
						</div>
					</div>
					<div className="col-xl-4 col-lg-6 col-md-6">
						<div className="card_dash">
							<div className="card_dash_left">
								<h5 style={{marginBottom:10}}>Number of importer</h5>
								<h2>1500</h2>
							</div>
							<div className="card_dash_right p-0">
								<img src="./images/dashboard/age19-29.png" alt="" style={{width:100}}/>
							</div>
						</div>
					</div>
					<div className="col-xl-4 col-lg-6 col-md-6">
						<div className="card_dash">
							<div className="card_dash_left">
								<h5 style={{marginBottom:10}}>Number of manufacturer</h5>
								<h2>2650</h2>
							</div>
							<div className="card_dash_right p-0">
								<img src="./images/dashboard/age30-39.png" alt="" style={{width:100}}/>
							</div>
						</div>
					</div> */}
					<div className="col-12">
						<div className="card card-mini analysis_card">
							<div className="card-body">
								<h3 className="mb-4 mt-0">Month Wise Registration Buyer-Seller</h3>
								<Line data={this.state.DashboardGraph} />
							</div>
						</div>
                    </div>
				</div>
			</div>
		</div>
        
	</div>
    <Footer/>
        </>
    );
  }
}
