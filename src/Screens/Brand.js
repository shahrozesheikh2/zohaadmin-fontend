import React, { Component } from 'react'
import Footer from '../components/Footer';
import Header from '../components/Header';
import Sidebar from '../components/Sidebar';
import axios from "axios";
import Select from 'react-select';
export default class Brand extends Component {
    constructor(props) {
        super(props);
        this.state = { token:localStorage.getItem('zohausertoken'),
            branddata:[],
            viewbrand:'',
            editcode:'',
            editname:'',
            editphonecode:'',
            name:'',
            code:'',
            image:'',
            file:'',
            phonecode:'',
            editid:'',
            awsid:'',
            awsurl:'',
            loading:true,
            editimage:''
        };
        this.getbrand();
        
    }
    componentDidMount() {
    }
    getbrand=()=>{
        this.setState({ editname:'',name:''})
        const url = global.url+'/api/brand?offset=0&limit=50';
        axios.get(url).then(response => {
            this.setState({branddata:response.data.result.data})
        }).catch(err=>{
        console.log(err)
        });
    }
    viewbrand=(id)=>{
        var header={
            "Authorization":this.state.token
        }
        const url = global.url+'/api/brand/'+id;
        axios.get(url,{headers:header}).then(response => {
            this.setState({viewbrand:response.data.result.data})
        }).catch(err=>{
        console.log(err)
        });
    }
    deletebrand=(id)=>{
        var header={
            "Authorization":this.state.token
        }
        const url = global.url+'/api/brand/'+id;
        axios.delete(url,{headers:header}).then(response => {
            this.getbrand();
        }).catch(err=>{
        console.log(err);
        alert(err.response.data.message)
        });
    }
    uploadimage=(image,addorupdate)=>{
        if(image){
            var myHeaders = new Headers();
            myHeaders.append("Authorization", this.state.token);
            var formdata = new FormData();
            formdata.append("file", image, "image.jpg");
            var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: formdata,
            redirect: 'follow'
            };
            fetch(global.url+"/api/upload-resource", requestOptions).then(response => response.text()).then(result => {
                var data=JSON.parse(result)
                this.setState({awsid:data.result.data[0].awsId,awsurl:data.result.data[0].path});
                if(addorupdate=='add'){
                    this.addbrand()
                }
                else{
                    this.updatebrand()
                }
            })
            .catch(error => console.log('error', error)); 
        }
        else{
            alert('Upload Brand Image');
        }
            
    }
    updatebrand=()=>{
        var header={
            "Authorization":this.state.token
        }
        var body={
            "name": this.state.editname,
            "awsKey": this.state.awsid,
            "awsUrl": this.state.awsurl,
        }
        const url = global.url+'/api/brand/'+this.state.editid;
            axios.put(url,body,{headers:header}).then(response => {
                console.log('====================================');
                console.log(response.data);
                console.log('====================================');
                this.setState({editname:'',awsid:'',awsurl:'',editimage:''})
                this.getbrand();
            }).catch(err=>{
            console.log(err);
            alert(err.response.data.message)
            });
        
    }
    addbrand=()=>{
        var header={
            "Authorization":this.state.token
        }
        var body={
            "name": this.state.name,
            "awsKey": this.state.awsid,
            "awsUrl": this.state.awsurl,
        }
        const url = global.url+'/api/brand/';
        axios.post(url,body,{headers:header}).then(response => {
            console.log('====================================');
            console.log(response.data);
            console.log('====================================');
            this.setState({name:'',awsid:'',awsurl:'',image:''});
            document.getElementById('uploadFile').value = "";
            this.getbrand();
        }).catch(err=>{
        console.log(err);
        alert(err.response.data.message)
        });
        
    }
    render() {
        return (
            <>
            <Header/>
            <Sidebar sidebarscreenname={'Brand'}/>
            <div className="wrapper">
                <div className="sa4d25">
                    <div className="container-fluid">			
                        <div className="row">
                            <div className="col-lg-12">	
                                <h2 className="st_title"><i className="uil uil-play-circle"></i>Brand</h2>
                            </div>	
                            <div className="col-lg-4">	
                            </div>
                            <div className="col-lg-4">	
                            </div>
                        </div>


                        <div className="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                            <div className="panel panel-default">
                                <div className="panel-heading" role="tab" id="headingOne">
                                    <div className="panel-title adcrse1250">
                                        <a className="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                            Add Brand
                                        </a>
                                    </div>
                                </div>
                                <div id="collapseOne" className="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                    <div className="panel-body adcrse_body">
                                        <div className="row">
                                            <div className="col-lg-12">
                                                <div className="discount_form">
                                                    <div className="row">
                                                        <div className="col-lg-12 col-md-12">	
                                                            <div className="ui search focus mt-20 lbel25">
                                                                <label>Brand Name*</label>
                                                                <div className="ui left icon input swdh19">
                                                                    <input className="prompt srch_explore" placeholder="Brand Name" type="text" 
                                                                    value={this.state.name} onChange={(e) => {this.setState({name:e.target.value})}} required/>															
                                                                </div>
                                                            </div>										
                                                        </div>
                                                        <div className="col-lg-12 col-md-12">	
                                                            <div className="ui search focus mt-20 lbel25">
                                                                <label>Brand Image*</label>
                                                                <div className="ui left icon input swdh19">
                                                                    <input className="prompt srch_explore" placeholder="Brand Image" type="file" id="uploadFile"
                                                                    // value={this.state.name}
                                                                    onChange={(e) => {
                                                                        this.setState({image:e.target.files[0]});
                                                                        // var file=e.target.files[0]
                                                                        // setTimeout(() => {
                                                                        //     this.uploadimage(file)
                                                                        // }, 100);
                                                                    }} 
                                                                    required/>															
                                                                </div>
                                                            </div>										
                                                        </div>
                                                        <div className="col-lg-12 col-md-12" onClick={()=>{this.uploadimage(this.state.image,'add')}}>	
                                                            <button className="discount_btn" type="submit">Save</button>										
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="table-responsive mt-30">
                            <table className="table ucp-table">
                                <thead className="thead-s">
                                    <tr>
                                    <th className="text-center" scope="col">#</th>
                                        <th className="text-center" scope="col">Brand Image</th>
                                        <th className="text-center" scope="col">Brand Name</th>
                                        <th className="text-center" scope="col">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.state.branddata.map((item,index)=>{
                                        return(
                                        <tr key={index}>    
                                            <td className="text-center">{index+1}</td>
                                            <td className="text-center">
                                            <img src={item.awsUrl} style={{width:20,height:20,objectFit:'contain'}} />
                                            </td>
                                            <td className="text-center">{item.name}</td>
                                            <td className="text-center">
                                                <a href="javascript:void(0)" title="Edit" className="gray-s" data-toggle="modal" data-target="#myModal" onClick={()=>{this.setState({
                                                    editid:item.id,
                                                    editname:item.name
                                                })}} ><i className="uil uil-edit-alt"></i></a>
                                                {/* <a href="javascript:void(0)" title="Delete" className="gray-s" onClick={()=>{this.deletebrand(item.id)}}><i className="uil uil-trash-alt"></i></a> */}
                                            </td>
                                        </tr>
                                        )
                                    })}
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>




            <div className="modal fade" id="myModal" role="dialog" >
                <div className="modal-dialog modal-lg" style={{maxWidth:1100}}>
                    <div className="modal-content">
                        <div className="modal-header">
                        <button type="button" className="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div className="modal-body" style={{paddingBottom:30}}>
                            <div className="col-md-12">
                                <h3 className="text-center">Edit Brand</h3>
                            </div>
                            <div className="col-lg-12 col-md-12">	
                                <div className="ui search focus mt-20 lbel25">
                                    <label>Brand Name*</label>
                                    <div className="ui left icon input swdh19">
                                        <input className="prompt srch_explore" placeholder="Brand Name" type="text" 
                                        value={this.state.editname} onChange={(e) => {this.setState({editname:e.target.value})}} required/>															
                                    </div>
                                </div>										
                            </div>
                            <div className="col-lg-12 col-md-12">	
                                <div className="ui search focus mt-20 lbel25">
                                    <label>Brand Image*</label>
                                    <div className="ui left icon input swdh19">
                                        <input className="prompt srch_explore" placeholder="Brand Image" type="file"
                                        onChange={(e) => {
                                            this.setState({editimage:e.target.files[0]});
                                        }} 
                                        required/>															
                                    </div>
                                </div>										
                            </div>
                            <div className="col-lg-12 col-md-12" onClick={()=>{this.uploadimage(this.state.editimage,'edit')}}>	
                                <button className="discount_btn"  data-dismiss="modal">Save</button>										
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <Footer/>
            </>
        )
    }
}
