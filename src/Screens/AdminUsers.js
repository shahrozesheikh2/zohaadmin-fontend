import React, { Component } from 'react';
import Footer from '../components/Footer';
import Header from '../components/Header';
import Sidebar from '../components/Sidebar';
import axios from "axios";
export default class AdminUser extends Component {
    constructor(props) {
        super(props);
        this.state = { token:localStorage.getItem('zohausertoken'),
            adminuserdata:[],
            viewadminuser:'',
            editcode:'',
            editname:'',
            editphonecode:'',
            name:'',
            code:'',
            phonecode:'',
            adminuserid:'',
            fname:'',lname:'',email:'',pswd:'',
            editfname:'',editlname:'',editemail:'',editpswd:'',editnewpswd:'',editoldpswd:'',
            searchfname:'',searchlname:'',searchemail:'',searchrole:'',
        };
        this.getadminuser();
        
    }
    componentDidMount() {
    }
    getadminuser=()=>{
        var header={
            "Authorization":this.state.token
        }
        const url = global.url+'/api/admin/?firstName='+this.state.searchfname+'&lastName='+this.state.searchlname+'&email=&statusIds[]=2&statusIds[]=3&statusIds[]=4';
        axios.get(url,{headers:header}).then(response => {
            console.log('====================================');
            console.log(response.data.result.data);
            console.log('====================================');
            this.setState({adminuserdata:response.data.result.data})
        }).catch(err=>{
        console.log(err)
        });
    }
    viewadminuser=(id)=>{
        var header={
            "Authorization":this.state.token
        }
        const url = global.url+'/api/adminuser/'+id;
        axios.get(url,{headers:header}).then(response => {
            console.log('====================================');
            console.log(response.data.result.data);
            console.log('====================================');
            this.setState({viewadminuser:response.data.result.data})
        }).catch(err=>{
        console.log(err)
        });
    }
    deleteadminuser=(id,status)=>{
        var header={
            "Authorization":this.state.token
        }
        const url = global.url+'/api/admin/activate/'+id;
        var body={}
        if(status=='Deactivated'){
            axios.patch(url,body,{headers:header}).then(response => {
                console.log('====================================');
                console.log(response.data.result.data);
                console.log('====================================');
                this.getadminuser();
            }).catch(err=>{
            console.log(err);
            alert(err.response.data.message)
            }); 
        }
        else{
            axios.delete(url,{headers:header}).then(response => {
                console.log('====================================');
                console.log(response.data.result.data);
                console.log('====================================');
                this.getadminuser();
            }).catch(err=>{
            console.log(err);
            alert(err.response.data.message)
            });
        }
    }
    updateadminuser=()=>{
        var header={
            "Authorization":this.state.token
        }
        var body={
            "id": this.state.adminuserid,
            "password": this.state.editpswd
        }
        const url = global.url+'/api/admin/reset-password';
        axios.post(url,body,{headers:header}).then(response => {
            console.log('====================================');
            console.log(response.data);
            console.log('====================================');
            this.getadminuser();
        }).catch(err=>{
        console.log(err);
            alert(err.response.data.message)
        });
    }
    addadminuser=()=>{
        var header={
            "Authorization":this.state.token
        }
        var body={
            "firstName": this.state.fname,
            "lastName": this.state.lname,
            "email": this.state.email,
            "password": this.state.pswd
        }
        const url = global.url+'/api/admin';
        axios.post(url,body,{headers:header}).then(response => {
            console.log('====================================');
            console.log(response.data);
            console.log('====================================');
            this.getadminuser();
            this.setState({fname:'',
                lname:'',
                email:'',
                pswd:''})
        }).catch(err=>{
        console.log(err);
        alert(err.response.data.message)
        });
    }
  render() {
    return (
        <>
        <Header/>
        <Sidebar sidebarscreenname={'AdminUser'}/>
        <div className="wrapper">
        <div className="sa4d25">
            <div className="container-fluid">			
                <div className="row">
                    <div className="col-lg-12">	
                        <h2 className="st_title"><i className="uil uil-user"></i>Admin Users</h2>
                    </div>
                    <div className="col-lg-4"></div>
                    <div className="col-lg-4">	
                        <div className="ui search">
                            <div className="ui left icon input swdh11 swdh15">
                                <input className="prompt srch_explore" type="search" autocomplete="chrome-off" placeholder="First Name" value={this.state.searchfname} onChange={(e) => {this.setState({searchfname:e.target.value});
                            setTimeout(() => {
                                this.getadminuser();
                            }, 100);}}/>
                                
                            </div>
                        </div>
                    </div>
                    
                    <div className="col-lg-4">	
                        <div className="ui search">
                            <div className="ui left icon input swdh11 swdh15">
                                <input className="prompt srch_explore" type="search" autocomplete="chrome-off" placeholder="Last Name" value={this.state.searchlname} onChange={(e) => {this.setState({searchlname:e.target.value});
                            setTimeout(() => {
                                this.getadminuser();
                            }, 100);}}/>
                                
                            </div>
                        </div>
                    </div>	
                            {/* <div className="col-lg-4">	
                                <div className="ui search focus">
                                    <div className="ui left icon input swdh11 swdh15">
                                        <input className="prompt srch_explore" type="email" placeholder="Email" value={this.state.searchemail} onChange={(e) => {this.setState({searchemail:e.target.value});
                                    setTimeout(() => {
                                        this.getadminuser();
                                    }, 100);}}/>
                                       
                                    </div>
                                </div>
                            </div> */}
                            {/* <div className="col-lg-3">	
                                <div className="ui search focus">
                                    <div className="ui left icon input swdh11 swdh15">
                                        <input className="prompt srch_explore" type="text" placeholder="Role" value={this.state.searchrole} onChange={(e) => {this.setState({searchrole:e.target.value});
                                    setTimeout(() => {
                                        this.getadminuser();
                                    }, 100);}}/>
                                       
                                    </div>
                                </div>
                            </div> */}
                </div>
                <div className="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <div className="panel panel-default">
                        <div className="panel-heading" role="tab" id="headingOne">
                            <div className="panel-title adcrse1250">
                                <a className="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                    Create Admin User
                                </a>
                            </div>
                        </div>
                        <div id="collapseOne" className="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                            <div className="panel-body adcrse_body">
                                <div className="row">
                                    <div className="col-lg-12">
                                        <div className="discount_form">
                                            <div className="row">
                                                <div className="col-lg-6 col-md-6">	
                                                    <div className="ui search focus mt-20 lbel25">
                                                        <label>First Name*</label>
                                                        <div className="ui left icon input swdh19">
                                                            <input className="prompt srch_explore datepicker-here" placeholder="First Name" type="text" required 
                                                            value={this.state.fname} onChange={(e) => {this.setState({fname:e.target.value})}}/>															
                                                        </div>
                                                    </div>										
                                                </div>
                                                <div className="col-lg-6 col-md-6">	
                                                    <div className="ui search focus mt-20 lbel25">
                                                        <label>Last Name*</label>
                                                        <div className="ui left icon input swdh19">
                                                            <input className="prompt srch_explore" type="text" name="text" placeholder="Last Name"  required 
                                                            value={this.state.lname} onChange={(e) => {this.setState({lname:e.target.value})}} />															
                                                        </div>
                                                    </div>										
                                                </div>
                                                <div className="col-lg-6 col-md-6">	
                                                    <div className="ui search focus mt-20 lbel25">
                                                        <label>Email*</label>
                                                        <div className="ui left icon input swdh19">
                                                            <input className="prompt srch_explore datepicker-here" type="email" placeholder="Email" required
                                                            value={this.state.email} onChange={(e) => {this.setState({email:e.target.value})}} />															
                                                        </div>
                                                    </div>										
                                                </div>
                                                <div className="col-lg-6 col-md-6">	
                                                    <div className="ui search focus mt-20 lbel25">
                                                        <label>Password*</label>
                                                        <div className="ui left icon input swdh19">
                                                            <input className="prompt srch_explore datepicker-here" type="password" placeholder="Password" required
                                                            value={this.state.pswd} onChange={(e) => {this.setState({pswd:e.target.value})}} />															
                                                        </div>
                                                    </div>										
                                                </div>
                                                <div className="col-lg-12 col-md-12" >	
                                                    <button className="discount_btn" onClick={()=>{this.addadminuser()}}> Save</button>										
                                                </div>
                                            </div>
                                        </div>
									</div>
									
								</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="table-responsive mt-30">
                    <table className="table ucp-table">
                        <thead className="thead-s">
                            <tr>
								<th className="text-center" scope="col">First Name</th>
								<th className="text-center" scope="col">Last Name</th>
                                <th className="text-center" scope="col">Email</th>
                                <th className="text-center" scope="col">Status</th>
                                <th className="text-center" scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        {this.state.adminuserdata.map((item,index)=>{
                            return(
                            <tr key={index}>    
                                <td className="text-center">{item.firstName}</td>
                                <td className="text-center">{item.lastName}</td>
                                <td className="text-center">{item.email}</td>
                                <td className="text-center"><a href="javascript:void(0)" title="Delete" style={{color:item.status!='Approved'?'red':'green'}} onClick={()=>{this.deleteadminuser(item.id,item.status)}}>{item.status}</a></td>
                                <td className="text-center">
                                    <a href="javascript:void(0)" onClick={()=>{this.setState({adminuserid: item.id})}} title="Change Password" className="gray-s" data-toggle="modal" data-target="#myModal" ><i className="uil uil-lock"></i></a>
                                </td>
                            </tr>
                            )
                        })}
                        </tbody>
                    </table>
                </div>
                
            </div>
		</div>
	</div>

        <div className="modal fade" id="myModal" role="dialog" >
            <div className="modal-dialog modal-lg" style={{maxWidth:1100}}>
                <div className="modal-content">
                    <div className="modal-header">
                    <button type="button" className="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div className="modal-body" style={{paddingBottom:30}}>
                        <div className="col-md-12">
                            <h3 className="text-center">Change Admin User Password</h3>
                        </div>	
                        <div className="col-lg-12 col-md-12">	
                            <div className="ui search focus mt-20 lbel25">
                                <label>New Password*</label>
                                <div className="ui left icon input swdh19">
                                    <input className="prompt srch_explore" placeholder="Password" type="text" 
                                    value={this.state.editpswd} onChange={(e) => {this.setState({editpswd:e.target.value})}} required/>															
                                </div>
                            </div>										
                        </div>
                        <div className="col-lg-12 col-md-12" onClick={()=>{this.updateadminuser()}}>	
                            <button className="discount_btn"  data-dismiss="modal">Save</button>										
                        </div>
                    </div>
                </div>
            </div>
        </div>
            
        <Footer/>
        </>
    );
  }
}
